package com.tadzikt.swiftevolution.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.tadzikt.swiftevolution.App;
import com.tadzikt.swiftevolution.Consts;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.addIcon("organism.png", Files.FileType.Internal);
		config.width = Consts.WINDOWED_WIDTH;
		config.height = Consts.WINDOWED_HEIGHT;
		config.title = Consts.APP_NAME;
		config.samples = Consts.MSAA_SAMPLES;
		new LwjglApplication(App.getInstance(), config);
	}
}

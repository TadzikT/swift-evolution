package com.tadzikt.swiftevolution;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.tadzikt.swiftevolution.assets.Assets;

import java.util.Random;

public class Utils {
    private static TextureRegion blankTextureRegion;

    static {
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Assets.getSkin().getColor("blank"));
        pixmap.fill();
        Texture texture = new Texture(pixmap);
        blankTextureRegion = new TextureRegion(texture);
        pixmap.dispose();
    }

    /**
     * @return TextureRegion made from one pixel with a blank (full white, 0xffffffff) color. Change batch color in
     *         Actor's draw() method to draw with a chosen color.
     */
    public static TextureRegion getBlankTextureRegion() {
        return blankTextureRegion;
    }

    public static float map(float valueCoordA, float startCoordA, float endCoordA, float startCoordB, float endCoordB) {
        float ratio = (endCoordB - startCoordB) / (endCoordA - startCoordA);
        return ratio * (valueCoordA - startCoordA) + startCoordB;
    }

    /**
     * @return a random number between 0 (inclusive) and the specified value (inclusive).
     */
    public static int randomInt(Random random, int range) {
        return random.nextInt(range + 1);
    }

    /**
     * @return a random int between start (inclusive) and end (inclusive).
     */
    public static int randomInt(Random random, int start, int end) {
        return start + random.nextInt(end - start + 1);
    }

    /**
     * @return a random number between 0 (inclusive) and the specified value (exclusive).
     */
    public static float randomFloat(Random random, float range) {
        return random.nextFloat() * range;
    }

    /**
     * @return a random float between start (inclusive) and end (exclusive).
     */
    public static float randomFloat(Random random, float start, float end) {
        return start + random.nextFloat() * (end - start);
    }
}
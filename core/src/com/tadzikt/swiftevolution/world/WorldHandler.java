package com.tadzikt.swiftevolution.world;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPart;
import com.tadzikt.swiftevolution.world.entities.joints.MuscleJoint;
import com.tadzikt.swiftevolution.world.entities.obstacles.Actorable;
import com.tadzikt.swiftevolution.world.entities.obstacles.Ground;
import com.tadzikt.swiftevolution.world.entities.organisms.Organism;
import com.tadzikt.swiftevolution.world.entities.organisms.OrganismDef;

public class WorldHandler {
    private World world;
    private Organism organism;
    private Array<Body> bodiesTemp = new Array<>();
    private Array<MuscleJoint> muscleJoints = new Array<>();
    private int physicsTick;
    private boolean disqualified = false;

    public WorldHandler() {
        world = new World(Consts.GRAVITY_VECTOR, true);
        new Ground(world, -600, 150, 800, 100, 0, false);
        new Ground(world, -600, 150, 100, 250, 0, false);
        new Ground(world, 65, 545, 1600, 100, 30, false);
        new Ground(world, -500, 3, 700, 100, 0, true);
        new Ground(world, 180, 15, 40, 100, 50, false);
        new Ground(world, 202, 55, 300, 100, 20, true);
        new Ground(world, 300, 56, 45, 30, 20, false);
        new Ground(world, 380, 90, 50, 100, 340, false);
        new Ground(world, 500, 80, 40, 100, 30, false);
        new Ground(world, 443, 400, 1100, 100, 30, true);
    }

    public void init(OrganismDef organismDef) {
        world.getBodies(bodiesTemp);
        for (Body body : bodiesTemp) {
            if (!(body.getUserData() instanceof Actorable)) {
                world.destroyBody(body);
            }
        }
        muscleJoints.clear();
        organism = new Organism(world, organismDef, muscleJoints);
        physicsTick = 1;
        disqualified = false;
    }

    public boolean update(float delta) {
        if (physicsTick >= Consts.SIMULATION_LENGTH_IN_TICKS) {
            return false;
        }
        reverseMuscleJoints();
        world.step(delta, Consts.VELOCITY_ITERATIONS, Consts.POSITION_ITERATIONS);
        physicsTick++;
        disqualified = isOrganismDisqualified();
        return !disqualified;
    }

    public void reverseMuscleJoints() {
        for (MuscleJoint muscleJoint : muscleJoints) {
            int intervalA = muscleJoint.getMuscleJointDef().getMotorIntervalA();
            int intervalB = muscleJoint.getMuscleJointDef().getMotorIntervalB();
            if (physicsTick % (intervalA + intervalB) == 0 || physicsTick % (intervalA + intervalB) == intervalA) {
                float maxMotorForce = muscleJoint.getJoint().getMaxMotorForce();
                muscleJoint.getJoint().setMaxMotorForce(maxMotorForce * -1f);
            }
        }
    }

    /**
     * This method also limits BodyParts' velocity.
     */
    private boolean isOrganismDisqualified() {
        float minX = Float.MAX_VALUE;
        float maxX = -Float.MAX_VALUE;
        float minY = Float.MAX_VALUE;
        float maxY = -Float.MAX_VALUE;
        for (BodyPart bodyPart : organism.getBodyParts()) {
            Body body = bodyPart.getBody();

            if (Float.isNaN(body.getLinearVelocity().x) || Float.isNaN(body.getLinearVelocity().y)) {
                return true;
            }

            Vector2 velocity = body.getLinearVelocity();
            if (velocity.len2() > Consts.BODY_PART_VELOCITY_LEN2_LIMIT) {
                body.setLinearVelocity(velocity.nor().scl((float) Math.sqrt(Consts.BODY_PART_VELOCITY_LEN2_LIMIT)));
            }

            Vector2 position = body.getPosition();
            if (position.x < minX) {
                minX = position.x;
            }
            if (position.x > maxX) {
                maxX = position.x;
            }
            if (position.y < minY) {
                minY = position.y;
            }
            if (position.y  > maxY) {
                maxY = position.y;
            }
        }

        Vector2 diff = new Vector2(maxX - minX, maxY - minY);
        if (diff.len2() > Consts.ORGANISM_MAX_LEGAL_DIFF_LEN2) {
            return true;
        }

        return false;
    }

    public float calculateFitness() {
        if (disqualified) {
            return Consts.DISQUALIFIED_FITNESS_VALUE;
        }
        float highestX = -Float.MAX_VALUE;
        for (BodyPart bodyPart : organism.getBodyParts()) {
            Body body = bodyPart.getBody();
            PolygonShape polygonShape = (PolygonShape) body.getFixtureList().get(0).getShape();
            for (int i = 0; i < polygonShape.getVertexCount(); i++) {
                Vector2 point = new Vector2();
                polygonShape.getVertex(i, point);
                point = body.getWorldPoint(point);
                if (point.x > highestX) {
                    highestX = point.x;
                }
            }
        }
        return highestX;
    }

    public World getWorld() {
        return world;
    }

    public Organism getOrganism() {
        return organism;
    }

    public Array<MuscleJoint> getMuscleJoints() {
        return muscleJoints;
    }

    public int getPhysicsTick() {
        return physicsTick;
    }
}

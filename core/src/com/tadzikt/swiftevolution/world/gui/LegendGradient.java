package com.tadzikt.swiftevolution.world.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.Utils;
import com.tadzikt.swiftevolution.assets.Assets;

public class LegendGradient extends Group {
    private static Texture texture = Utils.getBlankTextureRegion().getTexture();
    private static float thickness = 10f;
    private static float length = 140f;
    private Color lowest;
    private Color highest;
    private float[] spriteVertices;

    public LegendGradient(float x, float y, Color lowest, Color highest, float lowestValue, float highestValue) {
        this(x, y, lowest, highest);

        Label lowestLabel = Assets.getFont().getLabel(String.valueOf(lowestValue), Consts.SMALL_FONT_SIZE);
        lowestLabel.setPosition(-(lowestLabel.getWidth() / 2), thickness);
        addActor(lowestLabel);

        Label highestLabel = Assets.getFont().getLabel(String.valueOf(highestValue), Consts.SMALL_FONT_SIZE);
        highestLabel.setPosition(length - (highestLabel.getWidth() / 2), thickness);
        addActor(highestLabel);
    }

    public LegendGradient(float x, float y, Color lowest, Color highest) {
        setPosition(x, y);
        this.lowest = lowest;
        this.highest = highest;
        this.spriteVertices = buildSpriteVertices();
    }

    private float[] buildSpriteVertices() {
        float lowestColorFloat = lowest.toFloatBits();
        float highestColorFloat = highest.toFloatBits();
        return new float[] {getX() + 0f,     getY() + 0f,        lowestColorFloat,  0f, 0f,
                            getX() + length, getY() + 0f,        highestColorFloat, 0f, 0f,
                            getX() + length, getY() + thickness, highestColorFloat, 0f, 0f,
                            getX() + 0f,     getY() + thickness, lowestColorFloat,  0f, 0f};
    }

    /**
     * Batch doesn't get its color changed here, because this batch.draw() method doesn't use batch's color.
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, spriteVertices, 0, 20);
        super.draw(batch, parentAlpha * getColor().a);
    }
}

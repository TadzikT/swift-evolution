package com.tadzikt.swiftevolution.world.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.assets.Assets;

public class Legend extends Group {
    private static Color restitutionLowest = Assets.getSkin().getColor("restitutionLowest");
    private static Color restitutionHighest = Assets.getSkin().getColor("restitutionHighest");
    private static Color densityLowest = Assets.getSkin().getColor("densityLowest");
    private static Color densityHighest = Assets.getSkin().getColor("densityHighest");
    private static Color spring = Assets.getSkin().getColor("springJoint");
    private static Color bone = Assets.getSkin().getColor("boneJoint");
    private static Color muscle = Assets.getSkin().getColor("muscleJoint");
    private float gradientStartX = 130f;
    private float spacingY = 35f;

    public Legend (float x, float y) {
        setPosition(x, y);

        addLabel("Body parts:", Consts.BIG_FONT_SIZE, 7);
        addLabel("Restitution:", Consts.NORMAL_FONT_SIZE, 6);
        addGradient(6, restitutionLowest, restitutionHighest, Consts.BODY_PART_MIN_RESTITUTION, Consts.BODY_PART_MAX_RESTITUTION);
        addLabel("Density:", Consts.NORMAL_FONT_SIZE, 5);
        addGradient(5, densityLowest, densityHighest, Consts.BODY_PART_MIN_DENSITY, Consts.BODY_PART_MAX_DENSITY);
        addLabel("Joints:", Consts.BIG_FONT_SIZE, 4);
        addLabel("Spring:", Consts.NORMAL_FONT_SIZE, 3);
        addGradient(3, spring);
        addLabel("Bone:", Consts.NORMAL_FONT_SIZE, 2);
        addGradient(2, bone);
        addLabel("Muscle:", Consts.NORMAL_FONT_SIZE, 1);
        addGradient(1, muscle);
    }

    private void addLabel(String text, int fontSize, int spacingYMultiplier) {
        Label label = Assets.getFont().getLabel(text, fontSize);
        label.setY(spacingY * spacingYMultiplier);
        addActor(label);
    }

    private void addGradient(int spacingYMultiplier, Color lowest, Color highest, float lowestValue,
                             float highestValue) {
        addActor(new LegendGradient(gradientStartX, spacingY * spacingYMultiplier + 5f, lowest, highest, lowestValue,
                highestValue));
    }

    private void addGradient(int spacingYMultiplier, Color color) {
        addActor(new LegendGradient(gradientStartX, spacingY * spacingYMultiplier + 5f, color, color));
    }
}

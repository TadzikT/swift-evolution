package com.tadzikt.swiftevolution.world;

import com.badlogic.gdx.math.Vector2;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.Utils;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPartDef;

import java.util.Random;

public class BodyPartDefGenerator {
    private Random random;

    public void init(Random random) {
        this.random = random;
    }

    /**
     * @param position Position of the BodyPartDef that this BodyPartDef will be close to. Those two BodyPartDef
     *                 should be then connected with a joint. If the organism doesn't have any BodyPartDef, pass the
     *                 body starting position.
     * @param firstBody Whether this BodyPartDef will be a first body in the organism or not.
     */
   public BodyPartDef getRandomBodyPartDef(Vector2 position, boolean firstBody) {
       float[] vertices = getRandomVertices();
       Vector2 positionCopy = position.cpy();
       if (!firstBody) {
           positionCopy.set(getRandomPositionFrom(positionCopy));
       }
       float friction = Utils.randomFloat(random, Consts.BODY_PART_MIN_FRICTION, Consts.BODY_PART_MAX_FRICTION);
       float restitution = Utils.randomFloat(random, Consts.BODY_PART_MIN_RESTITUTION,
               Consts.BODY_PART_MAX_RESTITUTION);
       float density = Utils.randomFloat(random, Consts.BODY_PART_MIN_DENSITY, Consts.BODY_PART_MAX_DENSITY);
       return new BodyPartDef(positionCopy, vertices, friction, restitution, density);
   }

    private float[] getRandomVertices() {
        float[] vertices = new float[8];
        do {
            for (int i = 0; i < vertices.length; i++) {
                float delta = Consts.BODY_PART_VERTICES_MAX_POSITION;
                vertices[i] = Utils.randomFloat(random, -delta, delta);
            }
        } while (!isPolygonConvex(vertices));
        return vertices;
    }

    public boolean isPolygonConvex(float[] vertices) {
        boolean gotNegative = false;
        boolean gotPositive = false;
        for (int i = 0; i < vertices.length; i += 2) {
            float ax = vertices[i % vertices.length];
            float ay = vertices[(i + 1) % vertices.length];
            float bx = vertices[(i + 2) % vertices.length];
            float by = vertices[(i + 3) % vertices.length];
            float cx = vertices[(i + 4) % vertices.length];
            float cy = vertices[(i + 5) % vertices.length];
            float crossProduct = crossProductLength(ax, ay, bx, by, cx, cy);
            if (crossProduct < 0) {
                gotNegative = true;
            } else if (crossProduct > 0) {
                gotPositive = true;
            }
            if (gotNegative && gotPositive) return false;
        }

        // Protects against this crash: https://stackoverflow.com/questions/27092282/box2d-libgdx-polygon-shape
        // Also makes sure there's no small or too flat and small BodyParts
        Vector2 firstA = new Vector2(vertices[0], vertices[1]);
        Vector2 firstB = new Vector2(vertices[2], vertices[3]);
        Vector2 firstC = new Vector2(vertices[4], vertices[5]);
        Vector2 secondA = new Vector2(vertices[0], vertices[1]);
        Vector2 secondB = new Vector2(vertices[4], vertices[5]);
        Vector2 secondC = new Vector2(vertices[6], vertices[7]);
        if (getAreaOfATriangle(firstA, firstB, firstC) + getAreaOfATriangle(secondA, secondB, secondC) < 2f) {
            return false;
        }

        return true;
    }

    private float crossProductLength(float ax, float ay, float bx, float by, float cx, float cy) {
        float bAx = ax - bx;
        float bAy = ay - by;
        float bCx = cx - bx;
        float bCy = cy - by;
        return (bAx * bCy - bAy * bCx);
    }

    private Vector2 getRandomPositionFrom(Vector2 position) {
        float delta = Consts.BODY_PART_POSITION_DELTA;
        return position.add(Utils.randomFloat(random, -delta, delta), Utils.randomFloat(random, -delta, delta));
    }

    /**
     * @return random point from the quadrangle built from the provided vertices.
     */
    public Vector2 getRandomAnchor(float[] vertices) {
        Vector2 firstA = new Vector2(vertices[0], vertices[1]);
        Vector2 firstB = new Vector2(vertices[2], vertices[3]);
        Vector2 firstC = new Vector2(vertices[4], vertices[5]);
        Vector2 secondA = new Vector2(vertices[0], vertices[1]);
        Vector2 secondB = new Vector2(vertices[4], vertices[5]);
        Vector2 secondC = new Vector2(vertices[6], vertices[7]);
        float firstArea = getAreaOfATriangle(firstA, firstB, firstC);
        float secondArea = getAreaOfATriangle(secondA, secondB, secondC);
        float quadrangleArea = firstArea + secondArea;
        float firstCoverage = firstArea / quadrangleArea;
        if (random.nextFloat() < firstCoverage) {
            return getRandomPointFromTriangle(firstA, firstB, firstC);
        } else {
            return getRandomPointFromTriangle(secondA, secondB, secondC);
        }
    }

    private float getAreaOfATriangle(Vector2 a, Vector2 b, Vector2 c) {
        return Math.abs((a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y)) / 2);
    }

    /**
     * Warning: modifies arguments.
     */
    private Vector2 getRandomPointFromTriangle(Vector2 a, Vector2 b, Vector2 c) {
        float r1 = random.nextFloat();
        float r2 = random.nextFloat();
        c.scl((float) Math.sqrt(r1) * r2);
        b.scl((float) Math.sqrt(r1) * (1 - r2));
        a.scl(1 - (float) Math.sqrt(r1));
        return a.add(b).add(c);
    }
}
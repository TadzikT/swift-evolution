package com.tadzikt.swiftevolution.world;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.tadzikt.swiftevolution.App;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.assets.Assets;
import com.tadzikt.swiftevolution.world.gui.Legend;

import java.text.DecimalFormat;

public class WorldGuiStage extends Stage {
    private static DecimalFormat decimalFormat = Assets.getDecimalFormat();
    private static Color blank = Assets.getSkin().getColor("blank");
    private static Color disqualified = Assets.getSkin().getColor("disqualified");
    private WorldStage worldStage;
    private Label timeLabel = Assets.getFont().getLabel("1", Consts.BIG_FONT_SIZE);
    private Label fitnessLabel = Assets.getFont().getLabel("2", Consts.BIG_FONT_SIZE);

    public WorldGuiStage(WorldStage worldStage) {
        super(new FitViewport(Consts.TARGET_HEIGHT / 1.5f, Consts.TARGET_HEIGHT / 1.5f), App.getInstance().getBatch());
        this.worldStage = worldStage;

        addActor(new Legend(10f, 10f));

        timeLabel.setPosition(10, getViewport().getWorldHeight() - Consts.BIG_FONT_SIZE - 10);
        addActor(timeLabel);

        fitnessLabel.setPosition(10, getViewport().getWorldHeight() - Consts.BIG_FONT_SIZE * 2 - 20);
        addActor(fitnessLabel);
    }

    @Override
    public void act(float delta) {
        String time = decimalFormat.format(worldStage.getWorldHandler().getPhysicsTick() * Consts.DELTA_TIME);
        timeLabel.setText("Time: " + time + "s");

        float fitness = worldStage.getWorldHandler().calculateFitness();
        String fitnessFormated = decimalFormat.format(fitness);
        fitnessLabel.setText("Fitness: " + fitnessFormated + "m");
        fitnessLabel.setColor(fitness == Consts.DISQUALIFIED_FITNESS_VALUE ? disqualified : blank);

        super.act(delta);
    }
}
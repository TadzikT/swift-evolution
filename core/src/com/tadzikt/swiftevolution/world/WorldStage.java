package com.tadzikt.swiftevolution.world;

import box2dLight.ConeLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.tadzikt.swiftevolution.App;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.assets.Assets;
import com.tadzikt.swiftevolution.overview.BackgroundActor;
import com.tadzikt.swiftevolution.world.entities.MeterMarkerActor;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPart;
import com.tadzikt.swiftevolution.world.entities.joints.MuscleJoint;
import com.tadzikt.swiftevolution.world.entities.obstacles.Actorable;
import com.tadzikt.swiftevolution.world.entities.organisms.OrganismDef;
import com.tadzikt.swiftevolution.world.entities.organisms.OrganismGroup;

public class WorldStage extends Stage {
    private static Color background = Assets.getSkin().getColor("background");
    private static Color ambientLightColor = Assets.getSkin().getColor("ambientLight");
    private static Color pointLightColor = Assets.getSkin().getColor("pointLight");
    private WorldHandler worldHandler;
    private OrganismGroup organismGroup;
    private Box2DDebugRenderer box2DDebugRenderer = new Box2DDebugRenderer();
    private RenderMode renderMode = RenderMode.NORMAL;
    private RayHandler rayHandler;

    public enum RenderMode {
        NORMAL, DEBUG, BOTH;

        private static RenderMode[] vals = values();

        public RenderMode toggle() {
            return vals[(ordinal() + 1) % vals.length];
        }
    }

    public WorldStage() {
        super(new FitViewport(Consts.WORLD_STAGE_VIEWPORT_WIDTH, Consts.WORLD_STAGE_VIEWPORT_WIDTH),
                App.getInstance().getBatch());
        OrthographicCamera camera = (OrthographicCamera) getCamera();
        camera.zoom = 2f;

        addListener(new WorldStageListener(this));

        worldHandler = new WorldHandler();

        rayHandler = new RayHandler(worldHandler.getWorld());
        rayHandler.setAmbientLight(ambientLightColor);
        rayHandler.setBlurNum(Consts.BLUR_NUM);
        new ConeLight(rayHandler, Consts.CONE_LIGHT_RAYS, pointLightColor, 500, 450, 250, 300, 100);

        addActor(new BackgroundActor(background));
        Array<Body> bodies = new Array<>();
        worldHandler.getWorld().getBodies(bodies);
        for (Body body : bodies) {
            if (body.getUserData() instanceof Actorable) {
                addActor(((Actorable) body.getUserData()).getActor(rayHandler));
            }
        }
        addActor(new MeterMarkerActor());
    }

    public void init(OrganismDef organismDef) {
        for (Actor actor : getRoot().getChildren()) {
            if (actor instanceof OrganismGroup) {
                actor.remove();
            }
        }
        worldHandler.init(organismDef);
        organismGroup = new OrganismGroup(worldHandler.getOrganism());
        addActor(organismGroup);
    }

    public void infoMuscleSpeed() {
        System.out.println("-------------");
        for (MuscleJoint muscleJoint : worldHandler.getMuscleJoints()) {
            System.out.println(muscleJoint.getJoint().getJointSpeed());
        }
    }

    public void infoVelocity() {
        System.out.println("-------------");
        for (BodyPart bodyPart : worldHandler.getOrganism().getBodyParts()) {
            System.out.println(bodyPart.getBody().getLinearVelocity());
        }
    }
    @Override
    public void act(float delta) {
        worldHandler.update(delta);
        cameraUpdate();
        super.act(delta);
    }

    private void cameraUpdate() {
        float worldVertMinX = Float.MAX_VALUE;
        float worldVertMaxX = -Float.MAX_VALUE;
        float worldVertMinY = Float.MAX_VALUE;
        float worldVertMaxY = -Float.MAX_VALUE;

        for (BodyPart bodyPart : organismGroup.getOrganism().getBodyParts()) {
            Body body = bodyPart.getBody();
            PolygonShape polygonShape = (PolygonShape) body.getFixtureList().get(0).getShape();
            for (int i = 0; i < polygonShape.getVertexCount(); i++) {
                Vector2 point = new Vector2();
                polygonShape.getVertex(i, point);
                point = body.getWorldPoint(point);
                if (point.x < worldVertMinX) {
                    worldVertMinX = point.x;
                } else if (point.x > worldVertMaxX) {
                    worldVertMaxX = point.x;
                }
                if (point.y < worldVertMinY) {
                    worldVertMinY = point.y;
                } else if (point.y  > worldVertMaxY) {
                    worldVertMaxY = point.y;
                }
            }
        }

        Vector2 worldMiddle = new Vector2((worldVertMinX + worldVertMaxX) / 2, (worldVertMinY + worldVertMaxY) / 2);

        getCamera().position.x += (worldMiddle.x - getCamera().position.x) * 0.1f;
        getCamera().position.y += (worldMiddle.y - getCamera().position.y) * 0.1f;
    }

    @Override
    public void draw() {
        OrthographicCamera camera = (OrthographicCamera) getCamera();
        switch (renderMode) {
            case NORMAL:
                super.draw();
                break;
            case DEBUG:
                box2DDebugRenderer.render(worldHandler.getWorld(), camera.combined);
                break;
            case BOTH:
                super.draw();
                box2DDebugRenderer.render(worldHandler.getWorld(), camera.combined);
                break;
        }
        rayHandler.setCombinedMatrix(camera);
        rayHandler.useCustomViewport(getViewport().getScreenX(), getViewport().getScreenY(),
                getViewport().getScreenWidth(), getViewport().getScreenHeight());
        rayHandler.updateAndRender();
    }

    @Override
    public void dispose () {
        rayHandler.dispose();
        super.dispose();
    }

    public WorldHandler getWorldHandler() {
        return worldHandler;
    }

    public RenderMode getRenderMode() {
        return renderMode;
    }

    public void setRenderMode(RenderMode renderMode) {
        this.renderMode = renderMode;
    }
}
package com.tadzikt.swiftevolution.world;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.Utils;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPartDef;
import com.tadzikt.swiftevolution.world.entities.joints.AbstractJointDef;
import com.tadzikt.swiftevolution.world.entities.joints.BoneJointDef;
import com.tadzikt.swiftevolution.world.entities.joints.MuscleJointDef;
import com.tadzikt.swiftevolution.world.entities.joints.SpringJointDef;
import com.tadzikt.swiftevolution.world.entities.organisms.OrganismDef;
import com.tadzikt.swiftevolution.world.entities.organisms.OrganismGraph;

import java.util.Random;

public class OrganismDefGenerator {
    private static Vector2 bodyStartingPosition = Consts.SIMULATION_BODY_STARTING_POSITION;
    private Random random;
    private BodyPartDefGenerator bodyPartDefGenerator = new BodyPartDefGenerator();

    public void init(Random random) {
        this.random = random;
        bodyPartDefGenerator.init(this.random);
    }

    public OrganismDef generateRandomOrganismDef() {
        Array<BodyPartDef> bodyPartDefs = new Array<>();
        Array<AbstractJointDef> jointDefs = new Array<>();

        int bodyPartCount = Utils.randomInt(random, Consts.ORGANISM_MIN_BODY_PARTS, Consts.ORGANISM_MAX_BODY_PARTS);
        addRandomConnectedBodyPartDefs(bodyPartDefs, jointDefs, bodyPartCount);
        int jointCount = random.nextInt(bodyPartDefs.size);
        addRandomJointDefs(bodyPartDefs, jointDefs, jointCount);

        correctBodyPartDefsPosition(bodyPartDefs);
        return new OrganismDef(bodyPartDefs, jointDefs);
    }

    public OrganismDef mutateOrganismDef(OrganismDef organismDef) {
        Array<BodyPartDef> bodyPartDefs = new Array<>();
        Array<AbstractJointDef> jointDefs = new Array<>();
        organismDef.deepCopy(bodyPartDefs, jointDefs);

        removeJointDefs(bodyPartDefs, jointDefs, howMany(Consts.CHANCE_TO_REMOVE_JOINTS));
        removeBodyPartDefs(bodyPartDefs, jointDefs, howMany(Consts.CHANCE_TO_REMOVE_BODY_PARTS));
        mutateBodyPartDefs(bodyPartDefs, jointDefs, howMany(Consts.CHANCE_TO_MUTATE_BODY_PARTS));
        mutateJointDefs(jointDefs, howMany(Consts.CHANCE_TO_MUTATE_JOINTS));
        addRandomConnectedBodyPartDefs(bodyPartDefs, jointDefs, howMany(Consts.CHANCE_TO_ADD_BODY_PARTS));
        addRandomJointDefs(bodyPartDefs, jointDefs, howMany(Consts.CHANCE_TO_ADD_JOINTS));

        correctBodyPartDefsPosition(bodyPartDefs);
        return new OrganismDef(bodyPartDefs, jointDefs);
    }

    /**
     * Connected BodyPartDef means a BodyPartDef with a joint that connects this BodyPartDef to another BodyPartDef
     * from the array. If bodyPartDefs is empty, method creates a BodyPartDef without connecting joint.
     */
    private void addRandomConnectedBodyPartDefs(Array<BodyPartDef> bodyPartDefs, Array<AbstractJointDef> jointDefs,
                                                int count) {
        if (bodyPartDefs.size == 0) {
            bodyPartDefs.add(bodyPartDefGenerator.getRandomBodyPartDef(bodyStartingPosition, true));
            count--;
        }
        for (int i = 0; i < count; i++) {
            if (bodyPartDefs.size >= Consts.ORGANISM_MAX_BODY_PARTS) {
                return;
            }
            BodyPartDef chosen = bodyPartDefs.get(random.nextInt(bodyPartDefs.size));
            BodyPartDef newOne = bodyPartDefGenerator.getRandomBodyPartDef(chosen.getPosition(), false);
            bodyPartDefs.add(newOne);
            jointDefs.add(generateRandomJointDef(chosen, newOne));
        }
    }

    private void addRandomJointDefs(Array<BodyPartDef> bodyPartDefs, Array<AbstractJointDef> jointDefs, int count) {
        for (int i = 0; i < count; i++) {
            BodyPartDef first = bodyPartDefs.get(random.nextInt(bodyPartDefs.size));
            BodyPartDef second;
            do {
                second = bodyPartDefs.get(random.nextInt(bodyPartDefs.size));
            } while (first == second);
            jointDefs.add(generateRandomJointDef(first, second));
        }
    }

    private AbstractJointDef generateRandomJointDef(BodyPartDef first, BodyPartDef second) {
        switch(random.nextInt(3)) {
            case 0: return generateRandomSpringJointDef(first, second);
            case 1: return generateRandomBoneJointDef(first, second);
            case 2: return generateRandomMuscleJointDef(first, second);
        }
        return null;
    }

    private AbstractJointDef generateRandomSpringJointDef(BodyPartDef first, BodyPartDef second) {
        return generateRandomSpringJointDef(first, second, bodyPartDefGenerator.getRandomAnchor(first.getVertices()),
                bodyPartDefGenerator.getRandomAnchor(second.getVertices()));
    }

    private AbstractJointDef generateRandomSpringJointDef(BodyPartDef first, BodyPartDef second, Vector2 anchorA,
                                                          Vector2 anchorB) {
        float dampingRatio = Utils.randomFloat(random, Consts.JOINT_SPRING_MIN_DAMPING_RATIO,
                Consts.JOINT_SPRING_MAX_DAMPING_RATIO);
        float frequencyHz = Utils.randomFloat(random, Consts.JOINT_SPRING_MIN_FREQUENCY_HZ,
                Consts.JOINT_SPRING_MAX_FREQUENCY_HZ);
        return new SpringJointDef(first, second, anchorA, anchorB, frequencyHz, dampingRatio);
    }

    private AbstractJointDef generateRandomBoneJointDef(BodyPartDef first, BodyPartDef second) {
        return generateRandomBoneJointDef(first, second, bodyPartDefGenerator.getRandomAnchor(first.getVertices()),
                bodyPartDefGenerator.getRandomAnchor(second.getVertices()));
    }

    private AbstractJointDef generateRandomBoneJointDef(BodyPartDef first, BodyPartDef second, Vector2 anchorA,
                                                        Vector2 anchorB) {
        float lowerTranslationMultiplier = Utils.randomFloat(random,
                Consts.JOINT_BONE_MIN_LOWER_TRANSLATION_MULTIPLIER,
                Consts.JOINT_BONE_MAX_LOWER_TRANSLATION_MULTIPLIER);
        float upperTranslationMultiplier = Utils.randomFloat(random,
                Consts.JOINT_BONE_MIN_UPPER_TRANSLATION_MULTIPLIER,
                Consts.JOINT_BONE_MAX_UPPER_TRANSLATION_MULTIPLIER);
        return new BoneJointDef(first, second, anchorA, anchorB, lowerTranslationMultiplier,
                upperTranslationMultiplier);
    }

    private AbstractJointDef generateRandomMuscleJointDef(BodyPartDef first, BodyPartDef second) {
        return generateRandomMuscleJointDef(first, second, bodyPartDefGenerator.getRandomAnchor(first.getVertices()),
                bodyPartDefGenerator.getRandomAnchor(second.getVertices()));
    }

    private AbstractJointDef generateRandomMuscleJointDef(BodyPartDef first, BodyPartDef second, Vector2 anchorA,
                                                          Vector2 anchorB) {
        float lowerTranslationMultiplier = Utils.randomFloat(random,
                Consts.JOINT_MUSCLE_MIN_LOWER_TRANSLATION_MULTIPLIER,
                Consts.JOINT_MUSCLE_MAX_LOWER_TRANSLATION_MULTIPLIER);
        float upperTranslationMultiplier = Utils.randomFloat(random,
                Consts.JOINT_MUSCLE_MIN_UPPER_TRANSLATION_MULTIPLIER,
                Consts.JOINT_MUSCLE_MAX_UPPER_TRANSLATION_MULTIPLIER);
        float maxMotorForce = Utils.randomFloat(random, Consts.JOINT_MUSCLE_MIN_MAX_MOTOR_FORCE,
                Consts.JOINT_MUSCLE_MAX_MAX_MOTOR_FORCE);
        float motorSpeed = Utils.randomFloat(random, Consts.JOINT_MUSCLE_MIN_MOTOR_SPEED,
                Consts.JOINT_MUSCLE_MAX_MOTOR_SPEED);
        int motorIntervalA = Utils.randomInt(random, Consts.JOINT_MUSCLE_MIN_MOTOR_INTERVAL,
                Consts.JOINT_MUSCLE_MAX_MOTOR_INTERVAL);
        int motorIntervalB = Utils.randomInt(random, Consts.JOINT_MUSCLE_MIN_MOTOR_INTERVAL,
                Consts.JOINT_MUSCLE_MAX_MOTOR_INTERVAL);
        return new MuscleJointDef(first, second, anchorA, anchorB, lowerTranslationMultiplier,
                upperTranslationMultiplier, maxMotorForce, motorSpeed, motorIntervalA, motorIntervalB);
    }

    private void centerBodyPartDef(Array<BodyPartDef> bodyPartDefs) {
        float worldVertMinX = Float.MAX_VALUE;
        float worldVertMaxX = -Float.MAX_VALUE;
        float worldVertMinY = Float.MAX_VALUE;
        float worldVertMaxY = -Float.MAX_VALUE;

        for (BodyPartDef bodyPartDef : bodyPartDefs) {
            float[] vertices = bodyPartDef.getVertices();
            for (int i = 0; i < vertices.length; i += 2) {
                float worldVertX = bodyPartDef.getPosition().x + vertices[i];
                if (worldVertX < worldVertMinX) {
                    worldVertMinX = worldVertX;
                } else if (worldVertX > worldVertMaxX) {
                    worldVertMaxX = worldVertX;
                }
            }
            for (int i = 1; i < vertices.length; i += 2) {
                float worldVertY = bodyPartDef.getPosition().y + vertices[i];
                if (worldVertY < worldVertMinY) {
                    worldVertMinY = worldVertY;
                } else if (worldVertY > worldVertMaxY) {
                    worldVertMaxY = worldVertY;
                }
            }
        }

        Vector2 worldMiddle = new Vector2((worldVertMinX + worldVertMaxX) / 2, (worldVertMinY + worldVertMaxY) / 2);
        Vector2 moveBy = bodyStartingPosition.cpy().sub(worldMiddle);
        for (BodyPartDef bodyPartDef : bodyPartDefs) {
            bodyPartDef.getPosition().add(moveBy);
        }
    }

    private int howMany(float[] chances) {
        float randomFloat = random.nextFloat();
        for (int i = chances.length - 1; i >= 0; i--) {
            if (randomFloat < chances[i]) {
                return i + 1;
            }
        }
        return 0;
    }

    private void removeJointDefs(Array<BodyPartDef> bodyPartDefs, Array<AbstractJointDef> jointDefs, int count) {
        for (int i = 0; i < count; i++) {
            OrganismGraph organismGraph = new OrganismGraph(bodyPartDefs.size);
            Array<int[]> edges = new Array<>();
            for (int j = 0; j < jointDefs.size; j++) {
                AbstractJointDef jointDef = jointDefs.get(j);
                int vertexA = bodyPartDefs.indexOf(jointDef.getBodyPartDefA(), true);
                int vertexB = bodyPartDefs.indexOf(jointDef.getBodyPartDefB(), true);
                organismGraph.addEdge(vertexA, vertexB);
                edges.add(new int[]{vertexA, vertexB});
            }
            Array<int[]> bridges = organismGraph.findAllBridges();
            removeAllFrom(edges, bridges);
            if (edges.size == 0) {
                return;
            }
            int[] chosen = edges.get(Utils.randomInt(random, edges.size - 1));
            Array<AbstractJointDef> candidates = new Array<>();
            for (int j = 0; j < jointDefs.size; j++) {
                AbstractJointDef jointDef = jointDefs.get(j);
                int vertexA = bodyPartDefs.indexOf(jointDef.getBodyPartDefA(), true);
                int vertexB = bodyPartDefs.indexOf(jointDef.getBodyPartDefB(), true);
                if (areEdgesTheSame(chosen[0], chosen[1], vertexA, vertexB)) {
                    candidates.add(jointDef);
                }
            }
            AbstractJointDef toRemove;
            if (candidates.size == 1) {
                toRemove = candidates.peek();
            } else {
                toRemove = candidates.get(random.nextInt(candidates.size));
            }
            jointDefs.removeValue(toRemove, true);
        }
    }

    private void removeAllFrom(Array<int[]> edges, Array<int[]> bridges) {
        int size = edges.size;
        for (int i = 0; i < bridges.size; i++) {
            for (int j = 0; j < size; j++) {
                if (areEdgesTheSame(bridges.get(i)[0], bridges.get(i)[1], edges.get(j)[0], edges.get(j)[1])) {
                    edges.removeIndex(j);
                    size--;
                    break;
                }
            }
        }
    }

    private boolean areEdgesTheSame(int firstEdgeA, int firstEdgeB, int secondEdgeA, int secondEdgeB) {
        return ((firstEdgeA == secondEdgeA && firstEdgeB == secondEdgeB)
                || (firstEdgeA == secondEdgeB && firstEdgeB == secondEdgeA));
    }

    private void removeBodyPartDefs(Array<BodyPartDef> bodyPartDefs, Array<AbstractJointDef> jointDefs,
                                    int count) {
        for (int i = 0; i < count; i++) {
            if (bodyPartDefs.size <= Consts.ORGANISM_MIN_BODY_PARTS) {
                return;
            }
            OrganismGraph organismGraph = new OrganismGraph(bodyPartDefs.size);
            for (int j = 0; j < jointDefs.size; j++) {
                AbstractJointDef jointDef = jointDefs.get(j);
                int vertexA = bodyPartDefs.indexOf(jointDef.getBodyPartDefA(), true);
                int vertexB = bodyPartDefs.indexOf(jointDef.getBodyPartDefB(), true);
                organismGraph.addEdge(vertexA, vertexB);
            }
            Array<Integer> articulationPoints = organismGraph.findAllArticulationPoints();
            // This is probably not needed, I never saw it testing true, but just in case, I'm keeping it.
            if (articulationPoints.size == bodyPartDefs.size) {
                System.out.println("articulationPoints.size == bodyPartDefs.size, can't remove BodyPartDef");
                return;
            }
            Array<BodyPartDef> candidates = new Array<>();
            for (int j = 0; j < bodyPartDefs.size; j++) {
                boolean qualifying = true;
                for (int k = 0; k < articulationPoints.size; k++) {
                    if (j == articulationPoints.get(k)) {
                        qualifying = false;
                    }
                }
                if (qualifying) {
                    candidates.add(bodyPartDefs.get(j));
                }
            }
            BodyPartDef toRemove;
            if (candidates.size == 1) {
                toRemove = candidates.peek();
            } else {
                toRemove = candidates.get(random.nextInt(candidates.size));
            }
            Array<AbstractJointDef> jointDefsConnected = getJointDefsOfBodyPartDef(toRemove, jointDefs);
            jointDefs.removeAll(jointDefsConnected, true);
            bodyPartDefs.removeValue(toRemove, true);
        }
    }

    private Array<AbstractJointDef> getJointDefsOfBodyPartDef(BodyPartDef bodyPartDef,
                                                              Array<AbstractJointDef> jointDefs) {
        Array<AbstractJointDef> jointDefsConnected = new Array<>();
        for (int i = 0; i < jointDefs.size; i++) {
            if (jointDefs.get(i).getBodyPartDefA() == bodyPartDef
                    || jointDefs.get(i).getBodyPartDefB() == bodyPartDef) {
                jointDefsConnected.add(jointDefs.get(i));
            }
        }
        return jointDefsConnected;
    }

    private void correctBodyPartDefsPosition(Array<BodyPartDef> bodyPartDefs) {
        centerBodyPartDef(bodyPartDefs);
        for (BodyPartDef bodyPartDef : bodyPartDefs) {
            Vector2 position = bodyPartDef.getPosition();
            float overDistance = position.dst(bodyStartingPosition) - Consts.BODY_PART_MAX_DISTANCE_FROM_CENTER;
            if (overDistance > 0) {
                position.sub(bodyStartingPosition);
                position.setLength(Consts.BODY_PART_MAX_DISTANCE_FROM_CENTER);
                position.add(bodyStartingPosition);
            }
        }
        centerBodyPartDef(bodyPartDefs);
    }

    private void mutateBodyPartDefs(Array<BodyPartDef> bodyPartDefs, Array<AbstractJointDef> jointDefs, int count) {
        if (count > jointDefs.size) {
            count = jointDefs.size;
        }
        Array<Integer> alreadyMutated = new Array<>();
        for (int i = 0; i < count; i++) {
            int bodyPartIndex;
            do {
                bodyPartIndex = random.nextInt(bodyPartDefs.size);
            } while (alreadyMutated.contains(bodyPartIndex, true));
            alreadyMutated.add(bodyPartIndex);
            BodyPartDef bodyPartDef = bodyPartDefs.get(bodyPartIndex);
            if (random.nextFloat() < Consts.BODY_PART_CHANCE_TO_CHANGE_POSITION_OTHERWISE_PROPERTIES) {
                float delta = Consts.BODY_PART_POSITION_MUTATION_DELTA;
                bodyPartDef.setPosition(bodyPartDef.getPosition().add(Utils.randomFloat(random, -delta, delta),
                                                                      Utils.randomFloat(random, -delta, delta)));
            } else {
                float chance = random.nextFloat();
                if (chance < 0.25f) {
                    mutateVertices(bodyPartDef.getVertices());
                    correctAnchors(bodyPartDef, jointDefs);
                } else if (chance < 0.5f) {
                    bodyPartDef.setFriction(mutateProperty(
                            bodyPartDef.getFriction(),
                            Consts.BODY_PART_FRICTION_MUTATION_DELTA,
                            Consts.BODY_PART_MIN_FRICTION,
                            Consts.BODY_PART_MAX_FRICTION));
                } else if (chance < 0.75f) {
                    bodyPartDef.setRestitution(mutateProperty(
                            bodyPartDef.getRestitution(),
                            Consts.BODY_PART_RESTITUTION_MUTATION_DELTA,
                            Consts.BODY_PART_MIN_RESTITUTION,
                            Consts.BODY_PART_MAX_RESTITUTION));
                } else {
                    bodyPartDef.setDensity(mutateProperty(
                            bodyPartDef.getDensity(),
                            Consts.BODY_PART_DENSITY_MUTATION_DELTA,
                            Consts.BODY_PART_MIN_DENSITY,
                            Consts.BODY_PART_MAX_DENSITY));
                }
            }
        }
    }

    private void mutateVertices(float[] vertices) {
        float[] verticesCopy = new float[vertices.length];
        do {
            System.arraycopy(vertices, 0, verticesCopy, 0, vertices.length);
            for (int i = 0; i < vertices.length; i++) {
                verticesCopy[i] = mutateProperty(
                        vertices[i],
                        Consts.BODY_PART_VERTICES_POSITION_MUTATION_DELTA,
                        -Consts.BODY_PART_VERTICES_MAX_POSITION,
                        Consts.BODY_PART_VERTICES_MAX_POSITION);
            }
        } while (!bodyPartDefGenerator.isPolygonConvex(verticesCopy));
        System.arraycopy(verticesCopy, 0, vertices, 0, verticesCopy.length);
    }

    private void correctAnchors(BodyPartDef bodyPartDef, Array<AbstractJointDef> jointDefs) {
        Array<AbstractJointDef> connectedJoints = getJointDefsOfBodyPartDef(bodyPartDef, jointDefs);
        for (int i = 0; i < connectedJoints.size; i++) {
            Vector2 anchor;
            if (connectedJoints.get(i).getBodyPartDefA() == bodyPartDef) {
                anchor = connectedJoints.get(i).getAnchorA();
            } else {
                anchor = connectedJoints.get(i).getAnchorB();
            }
            anchor.set(bodyPartDefGenerator.getRandomAnchor(bodyPartDef.getVertices()));
        }
    }

    private void mutateJointDefs(Array<AbstractJointDef> jointDefs, int count) {
        if (count > jointDefs.size) {
            count = jointDefs.size;
        }
        Array<Integer> alreadyMutated = new Array<>();
        for (int i = 0; i < count; i++) {
            int jointIndex;
            do {
                jointIndex = random.nextInt(jointDefs.size);
            } while (alreadyMutated.contains(jointIndex, true));
            alreadyMutated.add(jointIndex);
            AbstractJointDef jointDef = jointDefs.get(jointIndex);
            if (random.nextFloat() < Consts.JOINT_CHANCE_TO_CHANGE_TYPE_OTHERWISE_PROPERTIES) {
                if (jointDef instanceof SpringJointDef) {
                    if (random.nextFloat() < 0.5f) {
                        jointDef = generateRandomBoneJointDef(jointDef.getBodyPartDefA(), jointDef.getBodyPartDefB(),
                                jointDef.getAnchorA(), jointDef.getAnchorB());
                    } else {
                        jointDef = generateRandomMuscleJointDef(jointDef.getBodyPartDefA(), jointDef.getBodyPartDefB(),
                                jointDef.getAnchorA(), jointDef.getAnchorB());
                    }
                } else if (jointDef instanceof BoneJointDef) {
                    if (random.nextFloat() < 0.5f) {
                        jointDef = generateRandomSpringJointDef(jointDef.getBodyPartDefA(), jointDef.getBodyPartDefB(),
                                jointDef.getAnchorA(), jointDef.getAnchorB());
                    } else {
                        jointDef = generateRandomMuscleJointDef(jointDef.getBodyPartDefA(), jointDef.getBodyPartDefB(),
                                jointDef.getAnchorA(), jointDef.getAnchorB());
                    }
                } else if (jointDef instanceof MuscleJointDef) {
                    if (random.nextFloat() < 0.5f) {
                        jointDef = generateRandomSpringJointDef(jointDef.getBodyPartDefA(), jointDef.getBodyPartDefB(),
                                jointDef.getAnchorA(), jointDef.getAnchorB());
                    } else {
                        jointDef = generateRandomBoneJointDef(jointDef.getBodyPartDefA(), jointDef.getBodyPartDefB(),
                                jointDef.getAnchorA(), jointDef.getAnchorB());
                    }
                }
                jointDefs.set(jointIndex, jointDef);
            } else {
                mutateAnchors(jointDef);
                if (jointDef instanceof SpringJointDef) {
                    mutateSpringJointDefProperties((SpringJointDef) jointDef);
                } else if (jointDef instanceof BoneJointDef) {
                    mutateBoneJointDefProperties((BoneJointDef) jointDef);
                } else if (jointDef instanceof MuscleJointDef) {
                    mutateMuscleJointDefProperties((MuscleJointDef) jointDef);
                }
            }
        }
    }

    private void mutateAnchors(AbstractJointDef jointDef) {
        int whichAnchors = howMany(Consts.JOINT_CHANCE_TO_MUTATE_ANCHORS);
        if (whichAnchors == 1) {
            jointDef.getAnchorA().set(bodyPartDefGenerator.getRandomAnchor(jointDef.getBodyPartDefA().getVertices()));
        } else if (whichAnchors == 2) {
            jointDef.getAnchorB().set(bodyPartDefGenerator.getRandomAnchor(jointDef.getBodyPartDefB().getVertices()));
        } else if (whichAnchors == 3) {
            jointDef.getAnchorA().set(bodyPartDefGenerator.getRandomAnchor(jointDef.getBodyPartDefA().getVertices()));
            jointDef.getAnchorB().set(bodyPartDefGenerator.getRandomAnchor(jointDef.getBodyPartDefB().getVertices()));
        }
    }

    private void mutateSpringJointDefProperties(SpringJointDef springJointDef) {
        springJointDef.setFrequencyHz(mutateProperty(
                springJointDef.getFrequencyHz(),
                Consts.JOINT_SPRING_FREQUENCY_HZ_MUTATION_DELTA,
                Consts.JOINT_SPRING_MIN_FREQUENCY_HZ,
                Consts.JOINT_SPRING_MAX_FREQUENCY_HZ));
        springJointDef.setDampingRatio(mutateProperty(
                springJointDef.getDampingRatio(),
                Consts.JOINT_SPRING_DAMPING_RATIO_MUTATION_DELTA,
                Consts.JOINT_SPRING_MIN_DAMPING_RATIO,
                Consts.JOINT_SPRING_MAX_DAMPING_RATIO));
    }

    private void mutateBoneJointDefProperties(BoneJointDef boneJointDef) {
        boneJointDef.setLowerTranslationMultiplier(mutateProperty(
                boneJointDef.getLowerTranslationMultiplier(),
                Consts.JOINT_BONE_LOWER_TRANSLATION_MULTIPLIER_MUTATION_DELTA,
                Consts.JOINT_BONE_MIN_LOWER_TRANSLATION_MULTIPLIER,
                Consts.JOINT_BONE_MAX_LOWER_TRANSLATION_MULTIPLIER));
        boneJointDef.setUpperTranslationMultiplier(mutateProperty(
                boneJointDef.getUpperTranslationMultiplier(),
                Consts.JOINT_BONE_UPPER_TRANSLATION_MULTIPLIER_MUTATION_DELTA,
                Consts.JOINT_BONE_MIN_UPPER_TRANSLATION_MULTIPLIER,
                Consts.JOINT_BONE_MAX_UPPER_TRANSLATION_MULTIPLIER));
    }

    private void mutateMuscleJointDefProperties(MuscleJointDef muscleJointDef) {
        muscleJointDef.setLowerTranslationMultiplier(mutateProperty(
                muscleJointDef.getLowerTranslationMultiplier(),
                Consts.JOINT_MUSCLE_LOWER_TRANSLATION_MULTIPLIER_MUTATION_DELTA,
                Consts.JOINT_MUSCLE_MIN_LOWER_TRANSLATION_MULTIPLIER,
                Consts.JOINT_MUSCLE_MAX_LOWER_TRANSLATION_MULTIPLIER));
        muscleJointDef.setUpperTranslationMultiplier(mutateProperty(
                muscleJointDef.getUpperTranslationMultiplier(),
                Consts.JOINT_MUSCLE_UPPER_TRANSLATION_MULTIPLIER_MUTATION_DELTA,
                Consts.JOINT_MUSCLE_MIN_UPPER_TRANSLATION_MULTIPLIER,
                Consts.JOINT_MUSCLE_MAX_UPPER_TRANSLATION_MULTIPLIER));
        muscleJointDef.setMaxMotorForce(mutateProperty(
                muscleJointDef.getMaxMotorForce(),
                Consts.JOINT_MUSCLE_MAX_MOTOR_FORCE_MUTATION_DELTA,
                Consts.JOINT_MUSCLE_MIN_MAX_MOTOR_FORCE,
                Consts.JOINT_MUSCLE_MAX_MAX_MOTOR_FORCE));
        muscleJointDef.setMotorSpeed(mutateProperty(
                muscleJointDef.getMotorSpeed(),
                Consts.JOINT_MUSCLE_MOTOR_SPEED_MUTATION_DELTA,
                Consts.JOINT_MUSCLE_MIN_MOTOR_SPEED,
                Consts.JOINT_MUSCLE_MAX_MOTOR_SPEED));
        muscleJointDef.setMotorIntervalA(mutateProperty(
                muscleJointDef.getMotorIntervalA(),
                Consts.JOINT_MUSCLE_MOTOR_INTERVAL_MUTATION_DELTA,
                Consts.JOINT_MUSCLE_MIN_MOTOR_INTERVAL,
                Consts.JOINT_MUSCLE_MAX_MOTOR_INTERVAL));
        muscleJointDef.setMotorIntervalB(mutateProperty(
                muscleJointDef.getMotorIntervalB(),
                Consts.JOINT_MUSCLE_MOTOR_INTERVAL_MUTATION_DELTA,
                Consts.JOINT_MUSCLE_MIN_MOTOR_INTERVAL,
                Consts.JOINT_MUSCLE_MAX_MOTOR_INTERVAL));
    }

    private float mutateProperty(float property, float delta, float min, float max) {
        return MathUtils.clamp(property + Utils.randomFloat(random, -delta, delta), min, max);
    }

    private int mutateProperty(int property, int delta, int min, int max) {
        return MathUtils.clamp(property + Utils.randomInt(random, -delta, delta), min, max);
    }
}
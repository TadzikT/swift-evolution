package com.tadzikt.swiftevolution.world.entities;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.Utils;
import com.tadzikt.swiftevolution.assets.Assets;

public class MeterMarkerActor extends Group {
    private static Color color = Assets.getSkin().getColor("meterMarker");
    private static TextureRegion textureRegion = Utils.getBlankTextureRegion();
    private static float frequencyInMeters = 20f;
    private static float markerThickness = 0.15f;
    private Label[] labels = new Label[5];

    public MeterMarkerActor() {
        for (int i = 0; i < labels.length; i++) {
            labels[i] = Assets.getFont().getLabel("", Consts.BIG_FONT_SIZE);
            labels[i].setFontScale(1 / Consts.ORGANISMS_STAGE_ZOOM);
            addActor(labels[i]);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

        Camera camera = getStage().getCamera();
        Vector2 position = new Vector2(camera.position.x - camera.viewportWidth,
                camera.position.y - camera.viewportHeight);
        Vector2 size = new Vector2(camera.viewportWidth * 2, camera.viewportHeight * 2);
        position.x -= frequencyInMeters; // we have to start rendering outside the viewport, so the labels won't clip
        size.x += frequencyInMeters;
        position.x += frequencyInMeters - (position.x % (frequencyInMeters));
        int i = 0;
        for (float x = position.x; x < position.x + size.x; x += frequencyInMeters) {
            float labelHeight = Consts.BIG_FONT_SIZE / Consts.ORGANISMS_STAGE_ZOOM;
            batch.draw(textureRegion, x - markerThickness / 2, position.y + labelHeight, markerThickness, size.y);
            String text = (int) x + "m";
            float labelWidth = Assets.getFont().getTextWidth(text, Consts.BIG_FONT_SIZE) / Consts.ORGANISMS_STAGE_ZOOM;
            labels[i].setText(text);
            labels[i].setPosition(x - labelWidth / 2, position.y + labelHeight / 2f);
            i++;
        }
        super.draw(batch, parentAlpha);
    }
}

package com.tadzikt.swiftevolution.world.entities.obstacles;

import box2dLight.RayHandler;
import com.badlogic.gdx.scenes.scene2d.Actor;

public interface Actorable {
    Actor getActor(RayHandler rayHandler);
}

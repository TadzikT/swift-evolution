package com.tadzikt.swiftevolution.world.entities.obstacles;

import box2dLight.ChainLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.assets.Assets;
import com.tadzikt.swiftevolution.assets.File;

public class GroundActor extends Actor {
    private static NinePatch ninePatch = Assets.getAtlas(File.SKIN).createPatch("ground");
    private static Color chainLightColor = Assets.getSkin().getColor("chainLight");
    private Ground ground;

    public GroundActor(RayHandler rayHandler, Ground ground) {
        this.ground = ground;
        setOrigin(ground.getHalfWidth(), ground.getHalfHeight());
        if (!ground.isNeon()) {
            return;
        }
        float[] vertices = new float[] {-ground.getHalfWidth(),
                                        ground.getHalfHeight() - 2,
                                        ground.getHalfWidth(),
                                        ground.getHalfHeight() - 2};
        int rays = (int) ((vertices[2] - vertices[0]) * Consts.CHAIN_LIGHT_RAY_PER_METER);
        ChainLight neon = new ChainLight(rayHandler, rays, chainLightColor, Consts.CHAIN_LIGHT_DISTANCE, 1, vertices);
        neon.attachToBody(ground.getBody());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        setBounds(ground.getBody().getPosition().x - ground.getHalfWidth(), ground.getBody().getPosition().y
                - ground.getHalfHeight(), ground.getHalfWidth() * 2, ground.getHalfHeight() * 2);
        setRotation(ground.getBody().getAngle() *  MathUtils.radiansToDegrees);
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        ninePatch.draw(batch, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), 1, 1,
                getRotation());
    }
}
package com.tadzikt.swiftevolution.world.entities.obstacles;

import box2dLight.RayHandler;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Ground implements Actorable {
    private World world;
    private Body body;
    private float halfWidth;
    private float halfHeight;
    private boolean neon;

    public Ground(World world, float x, float y, float width, float height, float angle, boolean neon) {
        this.world = world;

        halfWidth = width / 2;
        halfHeight = height / 2;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.KinematicBody;
        bodyDef.position.set(x + halfWidth, y - halfHeight);
        bodyDef.angle = (float) Math.toRadians(angle);
        body = this.world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(halfWidth, halfHeight);

        body.createFixture(shape, 0.0f);
        body.setUserData(this);
        shape.dispose();
        this.neon = neon;
    }

    @Override
    public Actor getActor(RayHandler rayHandler) {
        return new GroundActor(rayHandler, this);
    }

    public void rotate(boolean direction) {
        body.setAngularVelocity(direction ? 2 : -2);
    }

    public void stopRotating() {
        body.setAngularVelocity(0);
    }

    public World getWorld() {
        return world;
    }

    public Body getBody() {
        return body;
    }

    public float getHalfWidth() {
        return halfWidth;
    }

    public float getHalfHeight() {
        return halfHeight;
    }

    public boolean isNeon() {
        return neon;
    }
}
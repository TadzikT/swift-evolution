package com.tadzikt.swiftevolution.world.entities.bodyparts;

import com.badlogic.gdx.physics.box2d.*;

public class BodyPart {
    private World world;
    private BodyPartDef bodyPartDef;
    private Body body;

    public BodyPart(World world, BodyPartDef bodyPartDef) {
        this.world = world;
        this.bodyPartDef = bodyPartDef;
        if (this.world == null) {
            return;
        }

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(bodyPartDef.getPosition());
        body = this.world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.set(bodyPartDef.getVertices());

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.friction = bodyPartDef.getFriction();
        fixtureDef.restitution = bodyPartDef.getRestitution();
        fixtureDef.density = bodyPartDef.getDensity();
        fixtureDef.filter.groupIndex = -1;

        body.createFixture(fixtureDef);
        body.setUserData(this);
        polygonShape.dispose();
    }

    public World getWorld() {
        return world;
    }

    public Body getBody() {
        return body;
    }

    public BodyPartDef getBodyPartDef() {
        return bodyPartDef;
    }
}
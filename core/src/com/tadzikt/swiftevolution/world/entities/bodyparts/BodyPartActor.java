package com.tadzikt.swiftevolution.world.entities.bodyparts;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.Utils;
import com.tadzikt.swiftevolution.assets.Assets;

public class BodyPartActor extends Actor {
    private static TextureRegion textureRegion = Utils.getBlankTextureRegion();
    private static short[] triangles = new short[] {0, 1, 2, 0, 2, 3};
    private static Color restitutionLowestColor = Assets.getSkin().getColor("restitutionLowest");
    private static Color restitutionHighestColor = Assets.getSkin().getColor("restitutionHighest");
    private static Color densityLowestColor = Assets.getSkin().getColor("densityLowest");
    private static Color densityHighestColor = Assets.getSkin().getColor("densityHighest");
    private BodyPart bodyPart;
    private PolygonRegion polygonRegionBorder;
    private PolygonRegion polygonRegionFill;
    private Color borderColor;
    private Color fillColor;
    private boolean acting = true;

    public BodyPartActor(BodyPart bodyPart) {
        this.bodyPart = bodyPart;
        float[] vertices = this.bodyPart.getBodyPartDef().getVertices();
        polygonRegionBorder = new PolygonRegion(textureRegion, vertices, triangles);
        polygonRegionFill = new PolygonRegion(textureRegion, calculateInnerVertices(vertices), triangles);
        borderColor = mapColor(bodyPart.getBodyPartDef().getRestitution(), Consts.BODY_PART_MIN_RESTITUTION,
                Consts.BODY_PART_MAX_RESTITUTION, restitutionLowestColor, restitutionHighestColor);
        fillColor = mapColor(bodyPart.getBodyPartDef().getDensity(), Consts.BODY_PART_MIN_DENSITY,
                Consts.BODY_PART_MAX_DENSITY, densityLowestColor, densityHighestColor);
    }

    /**
     * @param v vertices of the outer quadrangle
     */
    private float[] calculateInnerVertices(float[] v) {
        Vector2 vA = calculateInnerVertex(v[6], v[7], v[0], v[1], v[2], v[3]);
        Vector2 vB = calculateInnerVertex(v[0], v[1], v[2], v[3], v[4], v[5]);
        Vector2 vC = calculateInnerVertex(v[2], v[3], v[4], v[5], v[6], v[7]);
        Vector2 vD = calculateInnerVertex(v[4], v[5], v[6], v[7], v[0], v[1]);
        return new float[] {vA.x, vA.y, vB.x, vB.y, vC.x, vC.y, vD.x, vD.y};
    }

    /**
     * Positive borderThickness value will produce vertex that is offseted to the inner side of the provided quadrangle.
     * Negative borderThickness value will produce vertex that is on the outer side of the quadrangle.
     */
    private Vector2 calculateInnerVertex(float prevX, float prevY, float curX, float curY, float nextX, float nextY) {
        Vector2 v = new Vector2(nextX - curX, nextY - curY);
        Vector2 w = new Vector2(prevX - curX, prevY - curY);
        Vector2 u = v.cpy().scl(w.len()).add(w.cpy().scl(v.len()));
        float absDetVW = Math.abs((v.x * w.y) - (v.y * w.x));
        return new Vector2(curX, curY).add(u.scl(Consts.BODY_PART_ACTOR_BORDER_THICKNESS / absDetVW));
    }

    private Color mapColor(float value, float min, float max, Color lowest, Color highest) {
        float r = Utils.map(value, min, max, lowest.r, highest.r);
        float g = Utils.map(value, min, max, lowest.g, highest.g);
        float b = Utils.map(value, min, max, lowest.b, highest.b);
        float a = Utils.map(value, min, max, lowest.a, highest.a);
        return new Color(r, g, b, a);
    }

    @Override
    public void act(float delta) {
        if (!acting) {
            return;
        }
        setPosition(bodyPart.getBody().getPosition().x, bodyPart.getBody().getPosition().y);
        setRotation(bodyPart.getBody().getAngle() * MathUtils.radiansToDegrees);
    }

    public void initForOrganismsStage() {
        setPosition(bodyPart.getBodyPartDef().getPosition().x, bodyPart.getBodyPartDef().getPosition().y);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        PolygonSpriteBatch polygonSpriteBatch = (PolygonSpriteBatch) batch;
        polygonSpriteBatch.setColor(borderColor.r, borderColor.g, borderColor.b, borderColor.a * parentAlpha);
        polygonSpriteBatch.draw(polygonRegionBorder, getX(), getY(), 0f, 0f, 1f, 1f, 1f, 1f, getRotation());
        polygonSpriteBatch.setColor(fillColor.r, fillColor.g, fillColor.b, fillColor.a * parentAlpha);
        polygonSpriteBatch.draw(polygonRegionFill, getX(), getY(), 0f, 0f, 1f, 1f, 1f, 1f, getRotation());
    }

    public void setActing(boolean acting) {
        this.acting = acting;
    }
}
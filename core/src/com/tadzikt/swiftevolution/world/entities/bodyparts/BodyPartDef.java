package com.tadzikt.swiftevolution.world.entities.bodyparts;

import com.badlogic.gdx.math.Vector2;

public class BodyPartDef {
    private Vector2 position;
    private float[] vertices;
    private float friction;
    private float restitution;
    private float density;

    public BodyPartDef(Vector2 position, float[] vertices, float friction, float restitution, float density) {
        this.position = position;
        this.vertices = vertices;
        this.friction = friction;
        this.restitution = restitution;
        this.density = density;
    }

    public BodyPartDef(BodyPartDef bodyPartDef) {
        this.position = bodyPartDef.getPosition().cpy();
        this.vertices = bodyPartDef.getVertices().clone();
        this.friction = bodyPartDef.getFriction();
        this.restitution = bodyPartDef.getRestitution();
        this.density = bodyPartDef.getDensity();
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public float[] getVertices() {
        return vertices;
    }

    public void setVertices(float[] vertices) {
        this.vertices = vertices;
    }

    public float getFriction() {
        return friction;
    }

    public void setFriction(float friction) {
        this.friction = friction;
    }

    public float getRestitution() {
        return restitution;
    }

    public void setRestitution(float restitution) {
        this.restitution = restitution;
    }

    public float getDensity() {
        return density;
    }

    public void setDensity(float density) {
        this.density = density;
    }
}
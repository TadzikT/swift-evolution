package com.tadzikt.swiftevolution.world.entities.organisms;

import com.badlogic.gdx.utils.Array;

public class OrganismGraph {
    private static final int NULL = -1;
    private int verticesCount;
    private Array<Integer>[] adjacencyList;
    private int visitCount = 0;

    public OrganismGraph(int verticesCount) {
        this.verticesCount = verticesCount;
        adjacencyList = new Array[verticesCount];
        for (int i = 0; i < verticesCount; i++) {
            adjacencyList[i] = new Array<>();
        }
    }

    public void addEdge(int vertexA, int vertexB) {
        adjacencyList[vertexA].add(vertexB);
        adjacencyList[vertexB].add(vertexA);
    }

    public Array<int[]> findAllBridges() {
        boolean[] visited = new boolean[verticesCount];
        int[] disc = new int[verticesCount];
        int[] low = new int[verticesCount];
        int[] parent = new int[verticesCount];
        Array<int[]> bridges = new Array<>();
        for (int i = 0; i < verticesCount; i++) {
            parent[i] = NULL;
        }
        for (int i = 0; i < verticesCount; i++) {
            if (!visited[i]) {
                visitVertexToFindBridges(i, visited, disc, low, parent, bridges);
            }
        }
        return bridges;
    }

    private void visitVertexToFindBridges(int vertex, boolean visited[], int disc[], int low[], int parent[],
                                          Array<int[]> bridges) {
        visited[vertex] = true;
        disc[vertex] = low[vertex] = ++visitCount;
        for (Integer v : adjacencyList[vertex]) {
            if (!visited[v]) {
                parent[v] = vertex;
                visitVertexToFindBridges(v, visited, disc, low, parent, bridges);
                low[vertex] = Math.min(low[vertex], low[v]);
                if (low[v] > disc[vertex]) {
                    bridges.add(new int[] {vertex, v});
                }
            } else if (v != parent[vertex]) {
                low[vertex] = Math.min(low[vertex], disc[v]);
            }
        }
    }

    public Array<Integer> findAllArticulationPoints() {
        boolean visited[] = new boolean[verticesCount];
        int disc[] = new int[verticesCount];
        int low[] = new int[verticesCount];
        int parent[] = new int[verticesCount];
        Array<Integer> articulationPoints = new Array<>();
        for (int i = 0; i < verticesCount; i++) {
            parent[i] = NULL;
        }
        for (int i = 0; i < verticesCount; i++) {
            if (!visited[i]) {
                visitVertexToFindArticulationPoints(i, visited, disc, low, parent, articulationPoints);
            }
        }
        return articulationPoints;
    }

    private void visitVertexToFindArticulationPoints(int vertex, boolean visited[], int disc[], int low[],
                                                     int parent[], Array<Integer> articulationPoints) {
        int children = 0;
        visited[vertex] = true;
        disc[vertex] = low[vertex] = ++visitCount;
        for (Integer v : adjacencyList[vertex]) {
            if (!visited[v]) {
                children++;
                parent[v] = vertex;
                visitVertexToFindArticulationPoints(v, visited, disc, low, parent, articulationPoints);
                low[vertex] = Math.min(low[vertex], low[v]);
                if ((parent[vertex] == NULL && children > 1) || (parent[vertex] != NULL && low[v] >= disc[vertex])) {
                    if (!articulationPoints.contains(vertex, false)) {
                        articulationPoints.add(vertex);
                    }
                }
            } else if (v != parent[vertex]) {
                low[vertex] = Math.min(low[vertex], disc[v]);
            }
        }
    }
}

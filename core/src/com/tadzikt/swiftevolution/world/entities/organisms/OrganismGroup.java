package com.tadzikt.swiftevolution.world.entities.organisms;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPart;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPartActor;
import com.tadzikt.swiftevolution.world.entities.joints.AbstractJoint;
import com.tadzikt.swiftevolution.world.entities.joints.JointActor;

public class OrganismGroup extends Group {
    private Organism organism;

    public OrganismGroup(Organism organism) {
        this.organism = organism;
        for (BodyPart bodyPart : organism.getBodyParts()) {
            addActor(new BodyPartActor(bodyPart));
        }
        for (AbstractJoint abstractJoint : organism.getJoints()) {
            addActor(new JointActor(abstractJoint));
        }
        setTouchable(Touchable.disabled);
    }

    public void initForOrganismsStage(float offsetX) {
        for (Actor actor : getChildren()) {
            if (actor instanceof BodyPartActor) {
                ((BodyPartActor) actor).initForOrganismsStage();
                ((BodyPartActor) actor).setActing(false);
            } else if (actor instanceof JointActor) {
                ((JointActor) actor).initForOrganismsStage();
                ((JointActor) actor).setActing(false);
            }
            actor.setX(actor.getX() + offsetX);
        }
    }

    public Organism getOrganism() {
        return organism;
    }
}
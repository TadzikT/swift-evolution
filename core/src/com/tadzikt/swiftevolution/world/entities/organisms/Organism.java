package com.tadzikt.swiftevolution.world.entities.organisms;

import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPart;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPartDef;
import com.tadzikt.swiftevolution.world.entities.joints.AbstractJoint;
import com.tadzikt.swiftevolution.world.entities.joints.AbstractJointDef;
import com.tadzikt.swiftevolution.world.entities.joints.MuscleJoint;

public class Organism {
    private World world;
    private OrganismDef organismDef;
    private Array<BodyPart> bodyParts;
    private Array<AbstractJoint> joints;

    /**
     * @param muscleJoints - pass null to skip adding MuscleJoints to the array
     */
    public Organism(World world, OrganismDef organismDef, Array<MuscleJoint> muscleJoints) {
        this.organismDef = organismDef;
        this.world = world;
        bodyParts = new Array<>();
        joints = new Array<>();
        for (BodyPartDef bodyPartDef : organismDef.getBodyPartDefs()) {
            bodyParts.add(new BodyPart(this.world, bodyPartDef));
        }
        for (AbstractJointDef jointDef : organismDef.getJointDefs()) {
            BodyPart bodyPartA = findBodyPart(jointDef.getBodyPartDefA());
            BodyPart bodyPartB = findBodyPart(jointDef.getBodyPartDefB());
            AbstractJoint abstractJoint = jointDef.createJoint(this.world, bodyPartA, bodyPartB);
            joints.add(abstractJoint);
            if (muscleJoints != null && abstractJoint instanceof MuscleJoint) {
                muscleJoints.add((MuscleJoint) abstractJoint);
            }
        }
    }

    private BodyPart findBodyPart(BodyPartDef searched) {
        for (BodyPart bodyPart : bodyParts) {
            BodyPartDef current = bodyPart.getBodyPartDef();
            if (current == searched) {
                return bodyPart;
            }
        }
        return null;
    }

    public OrganismDef getOrganismDef() {
        return organismDef;
    }

    public Array<BodyPart> getBodyParts() {
        return bodyParts;
    }

    public Array<AbstractJoint> getJoints() {
        return joints;
    }
}
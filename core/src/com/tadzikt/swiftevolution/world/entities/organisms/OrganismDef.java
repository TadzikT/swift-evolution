package com.tadzikt.swiftevolution.world.entities.organisms;

import com.badlogic.gdx.utils.Array;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPartDef;
import com.tadzikt.swiftevolution.world.entities.joints.AbstractJointDef;
import com.tadzikt.swiftevolution.world.entities.joints.BoneJointDef;
import com.tadzikt.swiftevolution.world.entities.joints.MuscleJointDef;
import com.tadzikt.swiftevolution.world.entities.joints.SpringJointDef;

public class OrganismDef {
    private Array<BodyPartDef> bodyPartDefs;
    private Array<AbstractJointDef> jointDefs;
    private float fitness = Float.NaN;
    private int bodyPartDefCount;
    private int springJointDefCount;
    private int boneJointDefCount;
    private int muscleJointDefCount;
    private int parentId = -1;

    public OrganismDef(Array<BodyPartDef> bodyPartDefs, Array<AbstractJointDef> jointDefs) {
        this.bodyPartDefs = bodyPartDefs;
        this.jointDefs = jointDefs;
        this.bodyPartDefCount = bodyPartDefs.size;
        for (AbstractJointDef abstractJointDef : jointDefs) {
            if (abstractJointDef instanceof SpringJointDef) {
                this.springJointDefCount++;
            } else if (abstractJointDef instanceof BoneJointDef) {
                this.boneJointDefCount++;
            } else if (abstractJointDef instanceof MuscleJointDef) {
                this.muscleJointDefCount++;
            }
        }
    }

    /**
     * Deep copies to those two arrays.
     */
    public void deepCopy(Array<BodyPartDef> emptyBodies, Array<AbstractJointDef> emptyJoints) {
        for (BodyPartDef bodyPartDef : bodyPartDefs) {
            emptyBodies.add(new BodyPartDef(bodyPartDef));
        }
        for (AbstractJointDef abstractJointDef : jointDefs) {
            int indexA = bodyPartDefs.indexOf(abstractJointDef.getBodyPartDefA(), true);
            int indexB = bodyPartDefs.indexOf(abstractJointDef.getBodyPartDefB(), true);
            emptyJoints.add(abstractJointDef.deepCopy(emptyBodies.get(indexA), emptyBodies.get(indexB)));
        }
    }

    public Array<BodyPartDef> getBodyPartDefs() {
        return bodyPartDefs;
    }

    public Array<AbstractJointDef> getJointDefs() {
        return jointDefs;
    }

    public float getFitness() {
        return fitness;
    }

    public void setFitness(float fitness) {
        this.fitness = fitness;
    }

    public int getBodyPartDefCount() {
        return bodyPartDefCount;
    }

    public int getSpringJointDefCount() {
        return springJointDefCount;
    }

    public int getBoneJointDefCount() {
        return boneJointDefCount;
    }

    public int getMuscleJointDefCount() {
        return muscleJointDefCount;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }
}
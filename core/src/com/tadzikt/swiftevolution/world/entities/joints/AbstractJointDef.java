package com.tadzikt.swiftevolution.world.entities.joints;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPart;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPartDef;

public abstract class AbstractJointDef {
    protected BodyPartDef bodyPartDefA;
    protected BodyPartDef bodyPartDefB;
    protected Vector2 anchorA;
    protected Vector2 anchorB;

    public AbstractJointDef(BodyPartDef bodyPartDefA, BodyPartDef bodyPartDefB, Vector2 anchorA, Vector2 anchorB) {
        this.bodyPartDefA = bodyPartDefA;
        this.bodyPartDefB = bodyPartDefB;
        this.anchorA = anchorA;
        this.anchorB = anchorB;
    }

    public abstract AbstractJoint createJoint(World world, BodyPart bodyPartA, BodyPart bodyPartB);

    public abstract AbstractJointDef deepCopy(BodyPartDef a, BodyPartDef b);

    public BodyPartDef getBodyPartDefA() {
        return bodyPartDefA;
    }

    public BodyPartDef getBodyPartDefB() {
        return bodyPartDefB;
    }

    public Vector2 getAnchorA() {
        return anchorA;
    }

    public Vector2 getAnchorB() {
        return anchorB;
    }
}
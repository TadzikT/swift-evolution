package com.tadzikt.swiftevolution.world.entities.joints;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef;
import com.tadzikt.swiftevolution.assets.Assets;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPart;

public class BoneJoint extends AbstractJoint {
    private static Color boneJointColor = Assets.getSkin().getColor("boneJoint");

    public BoneJoint(World world, BoneJointDef jointDef, BodyPart bodyPartA, BodyPart bodyPartB) {
        super(world, jointDef, bodyPartA, bodyPartB);
        if (world == null) {
            return;
        }

        PrismaticJointDef prismaticJointDef = new PrismaticJointDef();
        BoneJointDef boneJointDef = (BoneJointDef) this.abstractJointDef;
        float anchorsWorldDst = this.getAnchorsWorldDst();

        prismaticJointDef.bodyA = this.bodyPartA.getBody();
        prismaticJointDef.bodyB = this.bodyPartB.getBody();
        prismaticJointDef.localAnchorA.set(boneJointDef.anchorA);
        prismaticJointDef.localAnchorB.set(boneJointDef.anchorB);
        prismaticJointDef.localAxisA.set(getLocalAxisA());
        prismaticJointDef.referenceAngle = prismaticJointDef.bodyB.getAngle() - prismaticJointDef.bodyA.getAngle();
        prismaticJointDef.enableLimit = true;
        prismaticJointDef.lowerTranslation = anchorsWorldDst * boneJointDef.getLowerTranslationMultiplier();
        prismaticJointDef.upperTranslation = anchorsWorldDst * boneJointDef.getUpperTranslationMultiplier();
        this.world.createJoint(prismaticJointDef);
    }

    private Vector2 getLocalAxisA() {
        Vector2 worldAnchorA = this.bodyPartA.getBody().getWorldPoint(this.abstractJointDef.anchorA);
        Vector2 worldAnchorB = this.bodyPartB.getBody().getWorldPoint(this.abstractJointDef.anchorB);
        return worldAnchorB.sub(worldAnchorA).nor();
    }

    @Override
    public Color getColor() {
        return boneJointColor;
    }
}
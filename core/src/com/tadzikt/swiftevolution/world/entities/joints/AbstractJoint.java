package com.tadzikt.swiftevolution.world.entities.joints;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPart;

public abstract class AbstractJoint {
    protected World world;
    protected AbstractJointDef abstractJointDef;
    protected BodyPart bodyPartA;
    protected BodyPart bodyPartB;

    public AbstractJoint(World world, AbstractJointDef abstractJointDef, BodyPart bodyPartA, BodyPart bodyPartB) {
        this.world = world;
        this.abstractJointDef = abstractJointDef;
        this.bodyPartA = bodyPartA;
        this.bodyPartB = bodyPartB;
    }

    public abstract Color getColor();

    public Vector2 getWorldAnchorA() {
        return bodyPartA.getBody().getWorldPoint(abstractJointDef.anchorA);
    }

    public Vector2 getWorldAnchorB() {
        return bodyPartB.getBody().getWorldPoint(abstractJointDef.anchorB);
    }

    protected float getAnchorsWorldDst() {
        return getWorldAnchorA().dst(getWorldAnchorB());
    }

    /**
     * Only for non-moving JointActors (meaning the ones in OrganismsStage) because it doesn't take current body
     * position in simulation - only the position from BodyPartDef.
     */
    public Vector2 getAnchorA() {
        return bodyPartA.getBodyPartDef().getPosition().cpy().add(abstractJointDef.anchorA);
    }

    /**
     * Only for non-moving JointActors (meaning the ones in OrganismsStage) because it doesn't take current body
     * position in simulation - only the position from BodyPartDef.
     */
    public Vector2 getAnchorB() {
        return bodyPartB.getBodyPartDef().getPosition().cpy().add(abstractJointDef.anchorB);
    }
}
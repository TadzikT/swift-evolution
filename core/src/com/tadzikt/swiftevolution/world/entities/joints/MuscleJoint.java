package com.tadzikt.swiftevolution.world.entities.joints;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJoint;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef;
import com.tadzikt.swiftevolution.assets.Assets;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPart;

public class MuscleJoint extends AbstractJoint {
    private static Color muscleJointColor = Assets.getSkin().getColor("muscleJoint");
    private PrismaticJoint joint;

    public MuscleJoint(World world, MuscleJointDef jointDef, BodyPart bodyPartA, BodyPart bodyPartB) {
        super(world, jointDef, bodyPartA, bodyPartB);
        if (world == null) {
            return;
        }

        PrismaticJointDef prismaticJointDef = new PrismaticJointDef();
        MuscleJointDef muscleJointDef = (MuscleJointDef) this.abstractJointDef;
        float anchorsWorldDst = this.getAnchorsWorldDst();

        prismaticJointDef.bodyA = this.bodyPartA.getBody();
        prismaticJointDef.bodyB = this.bodyPartB.getBody();
        prismaticJointDef.localAnchorA.set(muscleJointDef.anchorA);
        prismaticJointDef.localAnchorB.set(muscleJointDef.anchorB);
        prismaticJointDef.localAxisA.set(getLocalAxisA());
        prismaticJointDef.referenceAngle = prismaticJointDef.bodyB.getAngle() - prismaticJointDef.bodyA.getAngle();
        prismaticJointDef.enableLimit = true;
        prismaticJointDef.lowerTranslation = anchorsWorldDst * muscleJointDef.getLowerTranslationMultiplier();
        prismaticJointDef.upperTranslation = anchorsWorldDst * muscleJointDef.getUpperTranslationMultiplier();
        prismaticJointDef.enableMotor = true;
        prismaticJointDef.maxMotorForce = muscleJointDef.getMaxMotorForce();
        prismaticJointDef.motorSpeed = muscleJointDef.getMotorSpeed();
        joint = (PrismaticJoint) this.world.createJoint(prismaticJointDef);
    }

    private Vector2 getLocalAxisA() {
        Vector2 worldAnchorA = this.bodyPartA.getBody().getWorldPoint(this.abstractJointDef.anchorA);
        Vector2 worldAnchorB = this.bodyPartB.getBody().getWorldPoint(this.abstractJointDef.anchorB);
        return worldAnchorB.sub(worldAnchorA).nor();
    }

    public PrismaticJoint getJoint() {
        return joint;
    }

    public MuscleJointDef getMuscleJointDef() {
        return (MuscleJointDef) this.abstractJointDef;
    }

    @Override
    public Color getColor() {
        return muscleJointColor;
    }
}

package com.tadzikt.swiftevolution.world.entities.joints;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.tadzikt.swiftevolution.assets.Assets;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPart;

public class SpringJoint extends AbstractJoint {
    private static Color springJointColor = Assets.getSkin().getColor("springJoint");

    public SpringJoint(World world, SpringJointDef jointDef, BodyPart bodyPartA, BodyPart bodyPartB) {
        super(world, jointDef, bodyPartA, bodyPartB);
        if (world == null) {
            return;
        }

        DistanceJointDef distanceJointDef = new DistanceJointDef();
        SpringJointDef springJointDef = (SpringJointDef) this.abstractJointDef;

        distanceJointDef.bodyA = this.bodyPartA.getBody();
        distanceJointDef.bodyB = this.bodyPartB.getBody();
        distanceJointDef.localAnchorA.set(springJointDef.getAnchorA());
        distanceJointDef.localAnchorB.set(springJointDef.getAnchorB());
        distanceJointDef.length = this.getAnchorsWorldDst();
        distanceJointDef.frequencyHz = springJointDef.getFrequencyHz();
        distanceJointDef.dampingRatio = springJointDef.getDampingRatio();
        this.world.createJoint(distanceJointDef);
    }

    @Override
    public Color getColor() {
        return springJointColor;
    }
}
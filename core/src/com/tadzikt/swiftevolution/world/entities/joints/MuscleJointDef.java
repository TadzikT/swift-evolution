package com.tadzikt.swiftevolution.world.entities.joints;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPart;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPartDef;

public class MuscleJointDef extends AbstractJointDef {
    private float lowerTranslationMultiplier;
    private float upperTranslationMultiplier;
    private float maxMotorForce;
    private float motorSpeed;
    private int motorIntervalA;
    private int motorIntervalB;

    public MuscleJointDef(BodyPartDef bodyPartDefA, BodyPartDef bodyPartDefB, Vector2 anchorA, Vector2 anchorB,
                          float lowerTranslationMultiplier, float upperTranslationMultiplier, float maxMotorForce,
                          float motorSpeed, int motorIntervalA, int motorIntervalB) {
        super(bodyPartDefA, bodyPartDefB, anchorA, anchorB);
        this.lowerTranslationMultiplier = lowerTranslationMultiplier;
        this.upperTranslationMultiplier = upperTranslationMultiplier;
        this.maxMotorForce = maxMotorForce;
        this.motorSpeed = motorSpeed;
        this.motorIntervalA = motorIntervalA;
        this.motorIntervalB = motorIntervalB;
    }

    @Override
    public MuscleJoint createJoint(World world, BodyPart bodyPartA, BodyPart bodyPartB) {
        return new MuscleJoint(world, this, bodyPartA, bodyPartB);
    }

    @Override
    public MuscleJointDef deepCopy(BodyPartDef a, BodyPartDef b) {
        return new MuscleJointDef(a, b, anchorA.cpy(), anchorB.cpy(), lowerTranslationMultiplier,
                upperTranslationMultiplier, maxMotorForce, motorSpeed, motorIntervalA, motorIntervalB);
    }

    public float getLowerTranslationMultiplier() {
        return lowerTranslationMultiplier;
    }

    public void setLowerTranslationMultiplier(float lowerTranslationMultiplier) {
        this.lowerTranslationMultiplier = lowerTranslationMultiplier;
    }

    public float getUpperTranslationMultiplier() {
        return upperTranslationMultiplier;
    }

    public void setUpperTranslationMultiplier(float upperTranslationMultiplier) {
        this.upperTranslationMultiplier = upperTranslationMultiplier;
    }

    public float getMaxMotorForce() {
        return maxMotorForce;
    }

    public void setMaxMotorForce(float maxMotorForce) {
        this.maxMotorForce = maxMotorForce;
    }

    public float getMotorSpeed() {
        return motorSpeed;
    }

    public void setMotorSpeed(float motorSpeed) {
        this.motorSpeed = motorSpeed;
    }

    public int getMotorIntervalA() {
        return motorIntervalA;
    }

    public void setMotorIntervalA(int motorIntervalA) {
        this.motorIntervalA = motorIntervalA;
    }

    public int getMotorIntervalB() {
        return motorIntervalB;
    }

    public void setMotorIntervalB(int motorIntervalB) {
        this.motorIntervalB = motorIntervalB;
    }
}
package com.tadzikt.swiftevolution.world.entities.joints;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPart;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPartDef;

public class SpringJointDef extends AbstractJointDef {
    private float frequencyHz;
    private float dampingRatio;

    public SpringJointDef(BodyPartDef bodyPartDefA, BodyPartDef bodyPartDefB, Vector2 anchorA, Vector2 anchorB,
                          float frequencyHz, float dampingRatio) {
        super(bodyPartDefA, bodyPartDefB, anchorA, anchorB);
        this.frequencyHz = frequencyHz;
        this.dampingRatio = dampingRatio;
    }

    @Override
    public SpringJoint createJoint(World world, BodyPart bodyPartA, BodyPart bodyPartB) {
        return new SpringJoint(world, this, bodyPartA, bodyPartB);
    }

    @Override
    public SpringJointDef deepCopy(BodyPartDef a, BodyPartDef b) {
       return new SpringJointDef(a, b, anchorA.cpy(), anchorB.cpy(), frequencyHz, dampingRatio);
    }

    public float getFrequencyHz() {
        return frequencyHz;
    }

    public void setFrequencyHz(float frequencyHz) {
        this.frequencyHz = frequencyHz;
    }

    public float getDampingRatio() {
        return dampingRatio;
    }

    public void setDampingRatio(float dampingRatio) {
        this.dampingRatio = dampingRatio;
    }
}
package com.tadzikt.swiftevolution.world.entities.joints;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.Utils;

public class JointActor extends Actor {
    private static TextureRegion textureRegion = Utils.getBlankTextureRegion();
    private AbstractJoint abstractJoint;
    private boolean acting = true;

    public JointActor(AbstractJoint abstractJoint) {
        this.abstractJoint = abstractJoint;
        setColor(this.abstractJoint.getColor());
    }

    @Override
    public void act(float delta) {
        if (!acting) {
            return;
        }
        setBoundsAndRotation(abstractJoint.getWorldAnchorA(), abstractJoint.getWorldAnchorB());
    }

    public void initForOrganismsStage() {
        setBoundsAndRotation(abstractJoint.getAnchorA(), abstractJoint.getAnchorB());
    }

    private void setBoundsAndRotation(Vector2 anchorA, Vector2 anchorB) {
        Vector2 anchorsDelta = new Vector2(anchorB).sub(anchorA);
        Vector2 perpendicular = new Vector2(anchorsDelta.y, -anchorsDelta.x).nor();
        Vector2 p1 = new Vector2();
        Vector2 p2 = new Vector2();
        p1.x = anchorA.x + perpendicular.x * Consts.JOINT_ACTOR_LINE_THICKNESS / 2;
        p1.y = anchorA.y + perpendicular.y * Consts.JOINT_ACTOR_LINE_THICKNESS / 2;
        p2.x = anchorA.x - perpendicular.x * Consts.JOINT_ACTOR_LINE_THICKNESS / 2;
        p2.y = anchorA.y - perpendicular.y * Consts.JOINT_ACTOR_LINE_THICKNESS / 2;

        setBounds(p1.x, p1.y, anchorA.dst(anchorB), p2.dst(p1));
        setRotation((float) Math.atan2(anchorB.y - anchorA.y, anchorB.x - anchorA.x) * MathUtils.radiansToDegrees);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setColor(getColor().r, getColor().g, getColor().b, getColor().a * parentAlpha);
        batch.draw(textureRegion, getX(), getY(), 0, 0, getWidth(), getHeight(), 1f, 1f, getRotation());
    }

    public void setActing(boolean acting) {
        this.acting = acting;
    }
}
package com.tadzikt.swiftevolution.world.entities.joints;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPart;
import com.tadzikt.swiftevolution.world.entities.bodyparts.BodyPartDef;

public class BoneJointDef extends AbstractJointDef {
    private float lowerTranslationMultiplier;
    private float upperTranslationMultiplier;

    public BoneJointDef(BodyPartDef bodyPartDefA, BodyPartDef bodyPartDefB, Vector2 anchorA, Vector2 anchorB,
                        float lowerTranslationMultiplier, float upperTranslationMultiplier) {
        super(bodyPartDefA, bodyPartDefB, anchorA, anchorB);
        this.lowerTranslationMultiplier = lowerTranslationMultiplier;
        this.upperTranslationMultiplier = upperTranslationMultiplier;
    }

    @Override
    public BoneJoint createJoint(World world, BodyPart bodyPartA, BodyPart bodyPartB) {
        return new BoneJoint(world, this, bodyPartA, bodyPartB);
    }

    @Override
    public BoneJointDef deepCopy(BodyPartDef a, BodyPartDef b) {
        return new BoneJointDef(a, b, anchorA.cpy(), anchorB.cpy(), lowerTranslationMultiplier,
                upperTranslationMultiplier);
    }

    public float getLowerTranslationMultiplier() {
        return lowerTranslationMultiplier;
    }

    public void setLowerTranslationMultiplier(float lowerTranslationMultiplier) {
        this.lowerTranslationMultiplier = lowerTranslationMultiplier;
    }

    public float getUpperTranslationMultiplier() {
        return upperTranslationMultiplier;
    }

    public void setUpperTranslationMultiplier(float upperTranslationMultiplier) {
        this.upperTranslationMultiplier = upperTranslationMultiplier;
    }
}
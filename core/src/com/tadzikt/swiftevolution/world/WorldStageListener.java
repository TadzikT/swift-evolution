package com.tadzikt.swiftevolution.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.tadzikt.swiftevolution.Consts;

public class WorldStageListener extends InputListener {
    private WorldStage worldStage;
    private OrthographicCamera camera;
    private boolean draggingCamera = false;

    public WorldStageListener(WorldStage worldStage) {
        this.worldStage = worldStage;
        this.camera = (OrthographicCamera) this.worldStage.getCamera();
    }

    @Override
    public boolean keyDown(InputEvent event, int keycode) {
        if (keycode == Input.Keys.TAB) {
            worldStage.setRenderMode(worldStage.getRenderMode().toggle());
            return true;
        } else if (keycode == Input.Keys.NUM_1) {
            worldStage.infoMuscleSpeed();
            return true;
        } else if (keycode == Input.Keys.NUM_2) {
            worldStage.infoVelocity();
            return true;
        }
        return false;
    }

    /**
     * There's a bug with Forward button. When you press it while holding other mouse button it's treated as if you
     * pressed the Back button. Thus there's the same action binded here for both Back and Forward mouse button.
     */
    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if (button == Input.Buttons.LEFT) {
            draggingCamera = true;
            return true;
        } else if (button == Input.Buttons.RIGHT) {
            worldStage.setRenderMode(worldStage.getRenderMode().toggle());
        } else if (button == Input.Buttons.MIDDLE) {
            camera.zoom = 1;
            camera.position.x = Consts.WORLD_STAGE_VIEWPORT_WIDTH / 2;
            camera.position.y = Consts.WORLD_STAGE_VIEWPORT_HEIGHT / 2;
            draggingCamera = false;
            camera.update();
        }
        return false;
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (button == Input.Buttons.LEFT) {
            draggingCamera = false;
        }
    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (draggingCamera) {
            camera.translate(new Vector2(-Gdx.input.getDeltaX(), Gdx.input.getDeltaY()).scl(camera.zoom * 0.03f));
            camera.update();
        }
    }

    @Override
    public boolean scrolled(InputEvent event, float x, float y, int amount) {
        camera.zoom *= (amount == -1 ? 0.9f : 10 / 9f);
        camera.update();
        return true;
    }
}
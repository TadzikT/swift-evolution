package com.tadzikt.swiftevolution;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.tadzikt.swiftevolution.assets.Assets;
import com.tadzikt.swiftevolution.overview.OverviewScreen;
import com.tadzikt.swiftevolution.title.TitleScreen;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class App extends Game {
	private static final App INSTANCE = new App();

	private PolygonSpriteBatch batch;
    private GlobalInputAdapter globalInputAdapter;
    private TitleScreen titleScreen;
    private OverviewScreen overviewScreen;
    private Random mainRandom;
    private Random[] randoms = new Random[Consts.SIMULATION_ORGANISMS_IN_A_GENERATION];
    private ExecutorService simulateThread;
    private int threadCount = Runtime.getRuntime().availableProcessors();
    private ExecutorService threadPool;

	private App() {
    }

	@Override
	public void create () {
        Assets.loadAssets();
        while (!Assets.getManager().update()) {
            // Waiting for assets to be loaded. This is bad code, but proper async loading needs way more code and I
            // don't load much anyway.
        }
        batch = new PolygonSpriteBatch();
        globalInputAdapter = new GlobalInputAdapter();
        titleScreen = new TitleScreen();
        overviewScreen = new OverviewScreen();
        simulateThread = Executors.newSingleThreadExecutor();
        threadPool = Executors.newFixedThreadPool(threadCount, r -> {
            Thread thread = new Thread(r);
            thread.setPriority(3);
            return thread;
        });
        Thread.currentThread().setPriority(10);
        setScreen(titleScreen);
	}

	@Override
	public void render () {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT
                | (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));
        screen.render(Consts.DELTA_TIME);
	}

	public void fromTitleToOverview(long mainRandomSeed) {
        mainRandom = mainRandomSeed == 0 ? new Random() : new Random(mainRandomSeed);
        for (int i = 0; i < randoms.length; i++) {
            randoms[i] = new Random(mainRandom.nextLong());
        }
        setScreen(overviewScreen);
    }

    public static App getInstance() {
        return INSTANCE;
    }

    public PolygonSpriteBatch getBatch() {
        return batch;
    }

    public GlobalInputAdapter getGlobalInputAdapter() {
        return globalInputAdapter;
    }

    public Random[] getRandoms() {
        return randoms;
    }

    public ExecutorService getSimulateThread() {
        return simulateThread;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public ExecutorService getThreadPool() {
        return threadPool;
    }
}
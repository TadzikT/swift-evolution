package com.tadzikt.swiftevolution;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;

public class GlobalInputAdapter extends InputAdapter {
    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.ENTER:
                if (Gdx.input.isKeyPressed(Input.Keys.ALT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.ALT_RIGHT)) {
                    if (Gdx.graphics.isFullscreen()) {
                        Gdx.graphics.setWindowedMode(Consts.WINDOWED_WIDTH, Consts.WINDOWED_HEIGHT);
                    } else {
                        Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
                    }
                    return true;
                }
                break;
            case Input.Keys.F4:
                if (Gdx.input.isKeyPressed(Input.Keys.ALT_LEFT)) {
                    Gdx.app.exit();
                }
                break;
        }
        return false;
    }
}
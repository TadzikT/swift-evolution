package com.tadzikt.swiftevolution.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Assets {
    private static AssetManager assetManager = new AssetManager();
    private static LazyBitmapFontController font = new LazyBitmapFontController();
    private static Skin skin = new Skin(Gdx.files.internal("skin.json"));
    private static DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public static void loadAssets() {
        for (File file : File.values()) {
            assetManager.load(file.getPath(), file.getType());
        }
        setSliderKnobs();
    }

    private static void setSliderKnobs() {
        String[] knobs = new String[] {"sliderKnob", "sliderDisabledKnob", "sliderKnobOver", "sliderKnobDown"};
        for (String knob : knobs) {
            NinePatchDrawable knobDrawable = (NinePatchDrawable) skin.getDrawable(knob);
            knobDrawable.setMinHeight(40);
            knobDrawable.setMinWidth(5);
        }
    }

    public static TextureAtlas getAtlas(File file) {
        return assetManager.get(file.getPath(), TextureAtlas.class);
    }

    public static <T> T get(File file) {
        return (T) file.getType().cast(assetManager.get(file.getPath(), file.getType()));
    }

    public static File[] getAll(Class type) {
        List<File> list = new ArrayList<>();
        for (File f : File.values()) {
            if (f.getType().equals(type)) {
                list.add(f);
            }
        }
        return list.toArray(new File[list.size()]);
    }

    public static AssetManager getManager() {
        return assetManager;
    }

    public static LazyBitmapFontController getFont() {
        return font;
    }

    public static Skin getSkin() {
        return skin;
    }

    public static DecimalFormat getDecimalFormat() {
        return decimalFormat;
    }
}
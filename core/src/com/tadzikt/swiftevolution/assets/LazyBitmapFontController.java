package com.tadzikt.swiftevolution.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Pools;

import java.util.Map;
import java.util.TreeMap;

public class LazyBitmapFontController {
    private Map<Integer, LazyBitmapFont> map = new TreeMap<>();

    LazyBitmapFontController() {
        LazyBitmapFont.setGenerator(new FreeTypeFontGenerator(Gdx.files.internal("openSansRegular.ttf")));
    }

    public LazyBitmapFont get(int fontSize) {
        LazyBitmapFont font = map.get(fontSize);
        if (font == null) {
            font = new LazyBitmapFont(fontSize);
            map.put(fontSize, font);
        }
        return font;
    }

    public Label getLabel(String text, int fontSize) {
        Label.LabelStyle ls = new Label.LabelStyle();
        ls.font = get(fontSize);
        return new Label(text, ls);
    }

    public Label getLabel(String text, int fontSize, Color color) {
        Label label = getLabel(text, fontSize);
        label.setColor(color);
        return label;
    }

    public Label getLabel(String text, int fontSize, String color) {
        Label label = getLabel(text, fontSize);
        label.setColor(Assets.getSkin().getColor(color));
        return label;
    }

    public Label getLabelWithMarkup(String text, int fontSize) {
        Label.LabelStyle ls = new Label.LabelStyle();
        ls.font = get(fontSize);
        ls.font.getData().markupEnabled = true;
        return new Label(text, ls);
    }

    public int getTextWidth(String str, int size) {
        return getTextWidth(get(size), str);
    }

    public int getTextWidth(BitmapFont font, String str) {
        GlyphLayout layout = Pools.obtain(GlyphLayout.class);
        layout.setText(font, str);
        return (int) layout.width;
    }
}
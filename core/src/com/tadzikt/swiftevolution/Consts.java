package com.tadzikt.swiftevolution;

import com.badlogic.gdx.math.Vector2;

public final class Consts {
    // System
    public static final String APP_NAME = "Swift Evolution";
    public static final int TARGET_WIDTH = 1920;
    public static final int TARGET_HEIGHT = 1080;
    public static final int WINDOWED_WIDTH = 1280;
    public static final int WINDOWED_HEIGHT = 720;
    public static final int MSAA_SAMPLES = 16;
    public static final float BOX2D_TO_WORLD = 50f;
    public static final float WORLD_STAGE_VIEWPORT_WIDTH = TARGET_WIDTH / BOX2D_TO_WORLD;
    public static final float WORLD_STAGE_VIEWPORT_HEIGHT = TARGET_HEIGHT / BOX2D_TO_WORLD;
    public static final float DELTA_TIME = 1 / 60f;

    // Font
    public static final int TITLE_FONT_SIZE = 120;
    public static final int GENERATION_TITLE_FONT_SIZE = 60;
    public static final int HUGE_FONT_SIZE = 30;
    public static final int BIG_FONT_SIZE = 24;
    public static final int NORMAL_FONT_SIZE = 18;
    public static final int SMALL_FONT_SIZE = 12;

    // Button
    public static final float BUTTON_WIDTH = 200f;
    public static final float BUTTON_HEIGHT = 60f;

    // Physics
    public static final Vector2 GRAVITY_VECTOR = new Vector2(0, -9.8f);
    public static final int VELOCITY_ITERATIONS = 8;
    public static final int POSITION_ITERATIONS = 3;

    // Simulation
    public static final Vector2 SIMULATION_BODY_STARTING_POSITION = new Vector2(0f, 20f);
    public static final int SIMULATION_LENGTH_IN_TICKS = 60 * 15;
    public static final int SIMULATION_ORGANISMS_IN_A_GENERATION = 1000;
    public static final float DISQUALIFIED_FITNESS_VALUE = -1000f;
    public static final float BODY_PART_VELOCITY_LEN2_LIMIT = 5000;
    public static final float ORGANISM_MAX_LEGAL_DIFF_LEN2 = 10000;

    // Box2DLights
    public static final int BLUR_NUM = 3;
    public static final float CHAIN_LIGHT_RAY_PER_METER = 8f;
    public static final float CHAIN_LIGHT_DISTANCE = 100f;
    public static final int CONE_LIGHT_RAYS = 10000;


    // Organism
    public static final int ORGANISM_MIN_BODY_PARTS = 3;
    public static final int ORGANISM_MAX_BODY_PARTS = 9;

    // BodyPart
    public static final float BODY_PART_POSITION_DELTA = 8f;
    public static final float BODY_PART_VERTICES_MAX_POSITION = 5f;
    public static final float BODY_PART_MIN_FRICTION = 0f;
    public static final float BODY_PART_MAX_FRICTION = 1f;
    public static final float BODY_PART_MIN_RESTITUTION = 0f;
    public static final float BODY_PART_MAX_RESTITUTION = 1f;
    public static final float BODY_PART_MIN_DENSITY = 1f;
    public static final float BODY_PART_MAX_DENSITY = 15f;
    public static final float BODY_PART_ACTOR_BORDER_THICKNESS = 0.2f;

    // Joint
    public static final float JOINT_SPRING_MIN_FREQUENCY_HZ = 1f;
    public static final float JOINT_SPRING_MAX_FREQUENCY_HZ = 30f;
    public static final float JOINT_SPRING_MIN_DAMPING_RATIO = 0f;
    public static final float JOINT_SPRING_MAX_DAMPING_RATIO = 1f;
    public static final float JOINT_BONE_MIN_LOWER_TRANSLATION_MULTIPLIER = 0.5f;
    public static final float JOINT_BONE_MAX_LOWER_TRANSLATION_MULTIPLIER = 1f;
    public static final float JOINT_BONE_MIN_UPPER_TRANSLATION_MULTIPLIER = 1.01f;
    public static final float JOINT_BONE_MAX_UPPER_TRANSLATION_MULTIPLIER = 1.5f;
    public static final float JOINT_MUSCLE_MIN_LOWER_TRANSLATION_MULTIPLIER = 0.5f;
    public static final float JOINT_MUSCLE_MAX_LOWER_TRANSLATION_MULTIPLIER = 1f;
    public static final float JOINT_MUSCLE_MIN_UPPER_TRANSLATION_MULTIPLIER = 1.01f;
    public static final float JOINT_MUSCLE_MAX_UPPER_TRANSLATION_MULTIPLIER = 2f;
    public static final float JOINT_MUSCLE_MIN_MAX_MOTOR_FORCE = 0f;
    public static final float JOINT_MUSCLE_MAX_MAX_MOTOR_FORCE = 30000f;
    public static final float JOINT_MUSCLE_MIN_MOTOR_SPEED = -60f;
    public static final float JOINT_MUSCLE_MAX_MOTOR_SPEED = 60f;
    public static final int JOINT_MUSCLE_MIN_MOTOR_INTERVAL = 15;
    public static final int JOINT_MUSCLE_MAX_MOTOR_INTERVAL = 300;
    public static final float JOINT_ACTOR_LINE_THICKNESS = 0.4f;

    // Mutation
    public static final float[] CHANCE_TO_REMOVE_JOINTS = new float[] {0.2f, 0.03f, 0.005f};
    public static final float[] CHANCE_TO_REMOVE_BODY_PARTS = new float[] {0.1f, 0.03f};
    public static final float[] CHANCE_TO_MUTATE_BODY_PARTS = new float[] {0.2f, 0.03f, 0.01f};
    public static final float[] CHANCE_TO_MUTATE_JOINTS = new float[] {0.2f, 0.03f, 0.01f};
    public static final float[] CHANCE_TO_ADD_BODY_PARTS = new float[] {0.1f, 0.03f};
    public static final float[] CHANCE_TO_ADD_JOINTS = new float[] {0.2f, 0.03f, 0.005f};
    public static final float BODY_PART_CHANCE_TO_CHANGE_POSITION_OTHERWISE_PROPERTIES = 0.3f;
    public static final float BODY_PART_POSITION_MUTATION_DELTA = 2f;
    public static final float BODY_PART_VERTICES_POSITION_MUTATION_DELTA = 1f;
    public static final float BODY_PART_FRICTION_MUTATION_DELTA = 0.05f;
    public static final float BODY_PART_RESTITUTION_MUTATION_DELTA = 0.08f;
    public static final float BODY_PART_DENSITY_MUTATION_DELTA = 0.5f;
    public static final float JOINT_CHANCE_TO_CHANGE_TYPE_OTHERWISE_PROPERTIES = 0.3f;
    public static final float[] JOINT_CHANCE_TO_MUTATE_ANCHORS = new float[] {0.3f, 0.05f, 0.01f};
    public static final float JOINT_SPRING_FREQUENCY_HZ_MUTATION_DELTA = 1f;
    public static final float JOINT_SPRING_DAMPING_RATIO_MUTATION_DELTA = 0.05f;
    public static final float JOINT_BONE_LOWER_TRANSLATION_MULTIPLIER_MUTATION_DELTA = 0.05f;
    public static final float JOINT_BONE_UPPER_TRANSLATION_MULTIPLIER_MUTATION_DELTA = 0.3f;
    public static final float JOINT_MUSCLE_LOWER_TRANSLATION_MULTIPLIER_MUTATION_DELTA = 0.05f;
    public static final float JOINT_MUSCLE_UPPER_TRANSLATION_MULTIPLIER_MUTATION_DELTA = 0.4f;
    public static final float JOINT_MUSCLE_MAX_MOTOR_FORCE_MUTATION_DELTA = 1000f;
    public static final float JOINT_MUSCLE_MOTOR_SPEED_MUTATION_DELTA = 8f;
    public static final int JOINT_MUSCLE_MOTOR_INTERVAL_MUTATION_DELTA = 15;
    public static final float BODY_PART_MAX_DISTANCE_FROM_CENTER = 11f;

    // OrganismsStage
    public static final int ORGANISMS_STAGE_LINE_SIZE = 8;
    public static final float ORGANISMS_STAGE_TABLE_WIDTH = 265f;
    public static final float ORGANISMS_STAGE_CELL_SIZE = ORGANISMS_STAGE_TABLE_WIDTH / ORGANISMS_STAGE_LINE_SIZE;
    public static final float ORGANISMS_STAGE_ZOOM = 8;
}
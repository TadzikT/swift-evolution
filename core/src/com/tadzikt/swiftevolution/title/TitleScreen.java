package com.tadzikt.swiftevolution.title;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.tadzikt.swiftevolution.App;

public class TitleScreen extends ScreenAdapter {
    private TitleStage titleStage  = new TitleStage(this);

    @Override
    public void show() {
        Gdx.input.setInputProcessor(new InputMultiplexer(App.getInstance().getGlobalInputAdapter(), titleStage));
    }

    @Override
    public void render(float delta) {
        titleStage.act(delta);
        titleStage.getViewport().apply();
        titleStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        titleStage.getViewport().update(width, height);
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        titleStage.dispose();
    }
}

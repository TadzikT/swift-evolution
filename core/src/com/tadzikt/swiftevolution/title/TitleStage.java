package com.tadzikt.swiftevolution.title;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.tadzikt.swiftevolution.App;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.assets.Assets;
import com.tadzikt.swiftevolution.overview.BackgroundActor;

public class TitleStage extends Stage {
    private static Color background = Assets.getSkin().getColor("background");
    private TitleScreen parent;
    private Label titleLabel = Assets.getFont().getLabel("Swift Evolution", Consts.TITLE_FONT_SIZE);
    private TitleOrganismActor titleOrganismActor = new TitleOrganismActor();
    private Table inputTable = new Table();
    private TextField seedTextField = new TextField("", Assets.getSkin());
    private TextButton okTextButton = new TextButton("OK", Assets.getSkin());

    public TitleStage(TitleScreen parent) {
        super(new FitViewport(Consts.TARGET_WIDTH, Consts.TARGET_HEIGHT), App.getInstance().getBatch());
        this.parent = parent;

        addActor(new BackgroundActor(background));

        titleLabel.setPosition(getWidth() / 2 - titleLabel.getWidth() / 2, getHeight() - 170);
        addActor(titleLabel);

        titleOrganismActor.setPosition(getWidth() / 2 - titleOrganismActor.getWidth() / 2, getHeight() - 370);
        addActor(titleOrganismActor);

        seedTextField.setMessageText("Enter seed");

        okTextButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                proceedToOverview();
            }
        });

        inputTable.add(seedTextField).size(700, Consts.BUTTON_HEIGHT);
        inputTable.add(okTextButton).size(Consts.BUTTON_WIDTH, Consts.BUTTON_HEIGHT);
        inputTable.setPosition(getWidth() / 2 - inputTable.getWidth() / 2, getHeight() - 670);
        addActor(inputTable);
    }

    @Override
    public boolean keyDown(int keyCode) {
        if (keyCode == Input.Keys.ENTER) {
            proceedToOverview();
        }
        return super.keyDown(keyCode);
    }

    public void proceedToOverview() {
        App.getInstance().fromTitleToOverview(seedTextField.getText().hashCode());
    }
}

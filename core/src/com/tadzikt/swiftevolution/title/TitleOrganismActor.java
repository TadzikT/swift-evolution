package com.tadzikt.swiftevolution.title;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.tadzikt.swiftevolution.assets.Assets;
import com.tadzikt.swiftevolution.assets.File;

public class TitleOrganismActor extends Actor {
    private static Sprite titleOrganism = Assets.getAtlas(File.SKIN).createSprite("titleOrganism");

    public TitleOrganismActor() {
        setSize(titleOrganism.getWidth(), titleOrganism.getHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(titleOrganism, getX(), getY(), titleOrganism.getWidth(), titleOrganism.getHeight());
    }
}

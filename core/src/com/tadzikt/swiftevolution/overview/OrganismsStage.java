package com.tadzikt.swiftevolution.overview;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.tadzikt.swiftevolution.App;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.assets.Assets;
import com.tadzikt.swiftevolution.world.entities.organisms.Organism;
import com.tadzikt.swiftevolution.world.entities.organisms.OrganismDef;
import com.tadzikt.swiftevolution.world.entities.organisms.OrganismGroup;

public class OrganismsStage extends Stage {
    private ScrollPane scrollPane;
    private Table table;

    public OrganismsStage(OverviewScreen parent) {
        super(new FitViewport(Consts.WORLD_STAGE_VIEWPORT_WIDTH, Consts.WORLD_STAGE_VIEWPORT_HEIGHT),
                App.getInstance().getBatch());

        OrthographicCamera camera = (OrthographicCamera) getCamera();
        camera.zoom = Consts.ORGANISMS_STAGE_ZOOM;
        camera.update();

        Vector2 position = screenToStageCoordinates(new Vector2(0, 0));
        table = new Table();
        table.defaults().size(Consts.ORGANISMS_STAGE_CELL_SIZE);
        for (int i = 1; i <= Consts.SIMULATION_ORGANISMS_IN_A_GENERATION; i++) {
            table.add(new OrganismTableCell(parent));
            if (i % Consts.ORGANISMS_STAGE_LINE_SIZE == 0) {
                table.row();
            }
        }

        scrollPane = new ScrollPane(table, Assets.getSkin());
        scrollPane.setBounds(position.x, -65, Consts.ORGANISMS_STAGE_TABLE_WIDTH + 5, 149);
        scrollPane.setFadeScrollBars(false);
        scrollPane.setFlickScrollTapSquareSize(0);
        addActor(scrollPane);
        setScrollFocus(scrollPane);
    }

    public void init(OrganismDef[] organismDefs) {
        for (int i = 0; i < Consts.SIMULATION_ORGANISMS_IN_A_GENERATION; i++) {
            OrganismGroup organismGroup = new OrganismGroup(new Organism(null, organismDefs[i], null));
            organismGroup.initForOrganismsStage(table.defaults().getPrefWidth() / 2);
            ((OrganismTableCell) table.getCells().get(i).getActor()).init(organismGroup, i + 1);
        }
    }
}

package com.tadzikt.swiftevolution.overview;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.tadzikt.swiftevolution.Utils;

public class BackgroundActor extends Actor {
    private static TextureRegion backgroundTexture = Utils.getBlankTextureRegion();

    public BackgroundActor(Color backgroundColor) {
        setColor(backgroundColor);
    }

    @Override
    public void act(float delta) {
        Camera camera = getStage().getCamera();
        setBounds(camera.position.x - camera.viewportWidth, camera.position.y - camera.viewportHeight,
                camera.viewportWidth * 2, camera.viewportHeight * 2);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setColor(getColor().r, getColor().g, getColor().b, getColor().a * parentAlpha);
        batch.draw(backgroundTexture, getX(), getY(), getWidth(), getHeight());
    }
}

package com.tadzikt.swiftevolution.overview;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ChartActor extends Actor {
    private Texture chartTexture;

    public ChartActor(int x, int y, Pixmap chartPixmap, boolean disposePixmap) {
        setBounds(x, y, chartPixmap.getWidth(), chartPixmap.getHeight());
        setChartTexture(chartPixmap, disposePixmap);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setColor(getColor().r, getColor().g, getColor().b, getColor().a * parentAlpha);
        batch.draw(chartTexture, getX(), getY(), getWidth(), getHeight());
    }

    public void setChartTexture(Pixmap chartPixmap, boolean dispose) {
        if (chartTexture != null) {
            chartTexture.dispose();
        }
        this.chartTexture = new Texture(chartPixmap);
        this.chartTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        if (dispose) {
            chartPixmap.dispose();
        }
    }
}

package com.tadzikt.swiftevolution.overview;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.tadzikt.swiftevolution.App;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.Utils;
import com.tadzikt.swiftevolution.assets.Assets;
import com.tadzikt.swiftevolution.world.OrganismDefGenerator;
import com.tadzikt.swiftevolution.world.WorldHandler;
import com.tadzikt.swiftevolution.world.entities.organisms.OrganismDef;
import org.knowm.xchart.*;
import org.knowm.xchart.internal.chartpart.Chart;
import org.knowm.xchart.style.AxesChartStyler;

import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class OverviewGuiStage extends Stage {
    private static com.badlogic.gdx.graphics.Color background = Assets.getSkin().getColor("background");
    private static Color awtBackground = new Color(Assets.getSkin().getColor("background").toIntBits());
    private static Color awtWidgetBorder = new Color(Assets.getSkin().getColor("widgetBackground").toIntBits());
    private static Color awtBlank = new Color(Assets.getSkin().getColor("blank").toIntBits());
    private OverviewScreen parent;
    private int threadCount = App.getInstance().getThreadCount();
    private OrganismDefGenerator[] organismDefGenerators = new OrganismDefGenerator[threadCount];
    private WorldHandler[] worldHandlers = new WorldHandler[threadCount];
    private AtomicInteger simulatedCount = new AtomicInteger();
    private boolean simulating = false;
    private GenerationHistory generationHistory = new GenerationHistory();
    private ChartActor xyChartActor;
    private double xData[] = new double[101];
    private Map<Double, Object> overrideMap = new HashMap<>();
    private ChartActor histogramActor;
    private Font font = new Font("SansSerif", Font.BOLD, 22);
    private Font font2 = new Font("SansSerif", Font.BOLD, 27);
    private Label generationLabel = Assets.getFont().getLabel("Generation 0/0", Consts.GENERATION_TITLE_FONT_SIZE);
    private Label simulatedPercentLabel = Assets.getFont().getLabel("", Consts.HUGE_FONT_SIZE);
    private TextButton simulateTextButton = new TextButton("Simulate", Assets.getSkin());
    private TextButton viewTextButton = new TextButton("View", Assets.getSkin());
    private Slider slider = new Slider(0f, 0f, 1f, false, Assets.getSkin());
    private int sliderValue;

    public OverviewGuiStage(OverviewScreen parent) {
        super(new FitViewport(Consts.TARGET_WIDTH, Consts.TARGET_HEIGHT), App.getInstance().getBatch());
        this.parent = parent;

        for (int i = 0; i < organismDefGenerators.length; i++) {
            organismDefGenerators[i] = new OrganismDefGenerator();
        }
        for (int i = 0; i < worldHandlers.length; i++) {
            worldHandlers[i] = new WorldHandler();
        }

        addActor(new BackgroundActor(background));

        xyChartActor = new ChartActor(20, 465, buildXyChartPixmap(), true);
        addActor(xyChartActor);

        for (int i = 0; i < xData.length; i++) {
            xData[i] = -500 + i * 15;
            if (i % 5 == 0) {
                overrideMap.put((double) i, (int) xData[i]);
            } else {
                overrideMap.put((double) i, " ");
            }
        }
        histogramActor = new ChartActor(20, 65, buildHistogramPixmap(-1), false);
        addActor(histogramActor);

        generationLabel.setPosition(20, Consts.TARGET_HEIGHT - Consts.GENERATION_TITLE_FONT_SIZE - 20);
        addActor(generationLabel);

        simulatedPercentLabel.setPosition(Consts.TARGET_WIDTH - 300,
                Consts.TARGET_HEIGHT - 80 + Consts.BUTTON_HEIGHT / 2f);
        addActor(simulatedPercentLabel);

        simulateTextButton.setBounds(Consts.TARGET_WIDTH - 220, Consts.TARGET_HEIGHT - 80, Consts.BUTTON_WIDTH,
                Consts.BUTTON_HEIGHT);
        addActor(simulateTextButton);

        viewTextButton.setBounds(Consts.TARGET_WIDTH - 220, Consts.TARGET_HEIGHT - 150, Consts.BUTTON_WIDTH,
                Consts.BUTTON_HEIGHT);
        viewTextButton.setDisabled(true);
        viewTextButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                view();
            }
        });
        addActor(viewTextButton);

        slider.setBounds(0, 0, Consts.TARGET_WIDTH, 60);
        slider.setDisabled(true);
        addActor(slider);

        addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode == Input.Keys.LEFT) {
                    slider.setValue(slider.getValue() - 1f);
                } else if (keycode == Input.Keys.RIGHT) {
                    slider.setValue(slider.getValue() + 1f);
                }
                return super.keyDown(event, keycode);
            }
        });
    }

    private void simulate() {
        App.getInstance().getSimulateThread().execute(() -> {
            long startTime = System.nanoTime();

            simulatedCount.set(0);
            simulating = true;
            OrganismDef[] organismDefs = new OrganismDef[Consts.SIMULATION_ORGANISMS_IN_A_GENERATION];
            List<Callable<Boolean>> callables = new ArrayList<>();
            for (int i = 0; i < threadCount; i++) {
                int a = i;
                callables.add(() -> simulationTask(a, organismDefs));
            }
            try {
                App.getInstance().getThreadPool().invokeAll(callables);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            long millis = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime);
            System.out.println("----------------------------------");
            System.out.println("All simulationTask() time: " + millis + " ms");

            Arrays.sort(organismDefs, (o1, o2) -> Float.compare(o2.getFitness(), o1.getFitness()));
            Generation generation = new Generation(organismDefs);
            System.out.println("Best fitness: " + generation.getBestFitness());
            System.out.println("Average fitness: " + generation.getAverageFitness());
            generationHistory.getGenerations().add(generation);

            setGenerationLabelText();
            Pixmap xyChartPixmap = buildXyChartPixmap();
            Gdx.app.postRunnable(() -> xyChartActor.setChartTexture(xyChartPixmap, true));
            Pixmap histogramPixmap = buildHistogramPixmap(generationHistory.getGenerations().size - 1);
            generationHistory.getGenerations().peek().setChartPixmap(histogramPixmap);
            slider.setRange(1f, generationHistory.getGenerations().size);
            slider.setDisabled(false);
            viewTextButton.setDisabled(false);
            simulating = false;
        });
    }

    private Boolean simulationTask(int thread, OrganismDef[] organismDefs) {
        int organismsPerThread = Consts.SIMULATION_ORGANISMS_IN_A_GENERATION / threadCount;
        int rangeStart = organismsPerThread * thread;
        int rangeEnd = thread == threadCount - 1 ?
                Consts.SIMULATION_ORGANISMS_IN_A_GENERATION : rangeStart + organismsPerThread;
        for (int i = rangeStart; i < rangeEnd; i++) {
            organismDefGenerators[thread].init(App.getInstance().getRandoms()[i]);
            worldHandlers[thread].init(getOrganismDefForWorldHandler(organismDefGenerators[thread], i));
            while (worldHandlers[thread].update(Consts.DELTA_TIME));
            organismDefs[i] = worldHandlers[thread].getOrganism().getOrganismDef();
            organismDefs[i].setFitness(worldHandlers[thread].calculateFitness());
            simulatedCount.incrementAndGet();
        }
        return Boolean.TRUE;
    }

    private OrganismDef getOrganismDefForWorldHandler(OrganismDefGenerator organismDefGenerator, int organismId) {
        if (generationHistory.getGenerations().size == 0) {
            return organismDefGenerator.generateRandomOrganismDef();
        } else {
            int parentId = organismId / 2;
            OrganismDef organismDef = organismDefGenerator
                    .mutateOrganismDef(generationHistory.getGenerations().peek().getOrganismDefs()[parentId]);
            organismDef.setParentId(parentId);
            return organismDef;
        }
    }

    private void view() {
        if (viewTextButton.isDisabled()) {
            return;
        }
        if (!parent.isShowOrganismsStage()) {
            initOrganismStage();
            parent.setShowOrganismsStage(true);
        } else {
            parent.setShowOrganismsStage(false);
            histogramActor.setChartTexture(generationHistory.getGenerations().get(sliderValue - 1)
                    .getChartPixmap(), false);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if ((int) slider.getValue() != sliderValue) {
            sliderValue = (int) slider.getValue();
            setGenerationLabelText();
            if (parent.isShowOrganismsStage()) {
                initOrganismStage();
            } else {
                histogramActor.setChartTexture(generationHistory.getGenerations().get(sliderValue - 1)
                        .getChartPixmap(), false);
            }
        }
        if (simulateTextButton.isChecked() && !simulating) {
            simulate();
        }
        setSimulatedPercentLabel();
        xyChartActor.setVisible(!parent.isShowOrganismsStage());
        histogramActor.setVisible(!parent.isShowOrganismsStage());
    }

    private Pixmap buildXyChartPixmap() {
        long startTime = System.nanoTime();

        XYChart chart = new XYChartBuilder().width(1700).height(530).title("Progress of the evolution")
                .xAxisTitle("Generations").yAxisTitle("Fitness in meters").build();
        stylize(chart.getStyler());

        if (generationHistory.getGenerations().size > 0) {
            float[] bestFitness = new float[generationHistory.getGenerations().size];
            for (int i = 0; i < generationHistory.getGenerations().size; i++) {
                bestFitness[i] = generationHistory.getGenerations().get(i).getBestFitness();
            }
            chart.addSeries("Best organism", bestFitness);
            addPercentileSeries(chart, 99, 95, 90, 70, 50, 20);
        }

        Pixmap pixmap = getChartPixmap(chart);

        long millis = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime);
        System.out.println("buildXyChartPixmap time: " + millis + " ms");

        return pixmap;
    }

    private void stylize(AxesChartStyler styler) {
        styler.setChartBackgroundColor(awtBackground);
        styler.setChartFontColor(awtBlank);
        styler.setAxisTickLabelsColor(awtBlank);
        styler.setAxisTickMarksColor(awtBlank);
        styler.setLegendBorderColor(awtWidgetBorder);
        styler.setLegendBackgroundColor(awtBackground);
        styler.setAxisTitleFont(font);
        styler.setLegendFont(font);
        styler.setAxisTickLabelsFont(font);
        styler.setChartTitleFont(font2);
        styler.setAxisTickMarkLength(5);
    }

    private void addPercentileSeries(XYChart chart, int... percentiles) {
        for (int percentile : percentiles) {
            float[] percentileData = new float[generationHistory.getGenerations().size];
            for (int i = 0; i < generationHistory.getGenerations().size; i++) {
                percentileData[i] = getFitnessForPercentile(i, percentile / 100f);
            }
            chart.addSeries(percentile + "th percentile", percentileData);
        }
    }

    private float getFitnessForPercentile(int generation, float percentile) {
        int index = (int) ((1 - percentile) * Consts.SIMULATION_ORGANISMS_IN_A_GENERATION);
        return generationHistory.getGenerations().get(generation).getOrganismDefs()[index].getFitness();
    }

    private Pixmap buildHistogramPixmap(int generation) {
        long startTime = System.nanoTime();

        CategoryChart chart = new CategoryChartBuilder().width(Consts.TARGET_WIDTH - 40).height(400)
                .title("Histogram of organisms in generation " + (generation + 1)).xAxisTitle("Meters")
                .yAxisTitle("Number of organisms").build();
        stylize(chart.getStyler());
        chart.getStyler().setYAxisMax(Consts.SIMULATION_ORGANISMS_IN_A_GENERATION * 0.5);

        double[] yData = new double[xData.length];
        if (generation >= 0) {
            OrganismDef[] organismDefs = generationHistory.getGenerations().get(generation).getOrganismDefs();
            for (int i = organismDefs.length - 1; i >= 0; i--) {
                float fitness = organismDefs[i].getFitness();
                for (int j = 0; j < xData.length - 1; j++) {
                    if (fitness < xData[j + 1]) {
                        yData[j]++;
                        break;
                    }
                }
                if (fitness >= xData[xData.length - 1]) {
                    yData[yData.length - 1]++;
                }
            }
        }
        chart.addSeries("Fitness", xData, yData);

        chart.setXAxisLabelOverrideMap(overrideMap);
        Pixmap pixmap = getChartPixmap(chart);

        long millis = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startTime);
        System.out.println("buildHistogramPixmap time: " + millis + " ms");
        return pixmap;
    }

    private Pixmap getChartPixmap(Chart chart) {
        byte[] bytes = new byte[0];
        try {
            bytes = BitmapEncoder.getBitmapBytes(chart, BitmapEncoder.BitmapFormat.PNG);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Pixmap(bytes, 0, bytes.length);
    }

    private void setGenerationLabelText() {
        generationLabel.setText("Generation " + sliderValue + "/" + generationHistory.getGenerations().size);
    }

    private void setSimulatedPercentLabel() {
        if (simulating) {
            simulatedPercentLabel.setVisible(true);
            float percent = Utils.map(simulatedCount.get(), 0, Consts.SIMULATION_ORGANISMS_IN_A_GENERATION, 0, 100);
            simulatedPercentLabel.setText((int) percent + "%");
        } else {
            simulatedPercentLabel.setVisible(false);
        }
    }

    private void initOrganismStage() {
        parent.getOrganismsStage().init(generationHistory.getGenerations().get(sliderValue - 1).getOrganismDefs());
    }
}

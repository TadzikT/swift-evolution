package com.tadzikt.swiftevolution.overview;

import com.badlogic.gdx.utils.Array;

public class GenerationHistory {
    private Array<Generation> generations = new Array<>();

    public Array<Generation> getGenerations() {
        return generations;
    }
}

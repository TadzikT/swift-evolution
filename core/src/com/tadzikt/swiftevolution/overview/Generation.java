package com.tadzikt.swiftevolution.overview;

import com.badlogic.gdx.graphics.Pixmap;
import com.tadzikt.swiftevolution.world.entities.organisms.OrganismDef;

public class Generation {
    private OrganismDef[] organismDefs;
    private Pixmap chartPixmap;
    private float averageFitness;

    public Generation(OrganismDef[] sortedOrganismDefs) {
        this.organismDefs = sortedOrganismDefs;
        float counter = 0f;
        for (OrganismDef organismDef : organismDefs) {
            counter += organismDef.getFitness();
        }
        averageFitness = counter / organismDefs.length;
    }

    public OrganismDef[] getOrganismDefs() {
        return organismDefs;
    }

    public Pixmap getChartPixmap() {
        return chartPixmap;
    }

    public void setChartPixmap(Pixmap chartPixmap) {
        this.chartPixmap = chartPixmap;
    }

    public float getBestFitness() {
        return organismDefs[0].getFitness();
    }

    public float getAverageFitness() {
        return averageFitness;
    }
}

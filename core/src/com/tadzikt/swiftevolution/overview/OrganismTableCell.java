package com.tadzikt.swiftevolution.overview;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.tadzikt.swiftevolution.Consts;
import com.tadzikt.swiftevolution.Utils;
import com.tadzikt.swiftevolution.assets.Assets;
import com.tadzikt.swiftevolution.world.entities.organisms.OrganismDef;
import com.tadzikt.swiftevolution.world.entities.organisms.OrganismGroup;

public class OrganismTableCell extends Group {
    private static float spacingY = Consts.NORMAL_FONT_SIZE / Consts.ORGANISMS_STAGE_ZOOM;
    private static String bodyPartColorHex = Assets.getSkin().getColor("bodyPart").toString();
    private static String springJointColorHex = Assets.getSkin().getColor("springJoint").toString();
    private static String boneJointColorHex = Assets.getSkin().getColor("boneJoint").toString();
    private static String muscleJointColorHex = Assets.getSkin().getColor("muscleJoint").toString();
    private static TextureRegion background = Utils.getBlankTextureRegion();
    private static Color backgroundColor = Assets.getSkin().getColor("cellMouseOver");
    private static Color blank = Assets.getSkin().getColor("blank");
    private static Color disqualified = Assets.getSkin().getColor("disqualified");
    private static float pad = 1f;
    private OverviewScreen overviewScreen;
    private OrganismGroup organismGroup;
    private Label placeLabel = Assets.getFont().getLabel("", Consts.NORMAL_FONT_SIZE);
    private Label fitnessLabel = Assets.getFont().getLabel("", Consts.NORMAL_FONT_SIZE);
    private Label speciesLabel = Assets.getFont().getLabelWithMarkup("", Consts.HUGE_FONT_SIZE);
    private Label parentIdLabel = Assets.getFont().getLabel("", Consts.NORMAL_FONT_SIZE);
    private ClickListener clickListener;

    public OrganismTableCell(OverviewScreen overviewScreen) {
        this.overviewScreen = overviewScreen;

        placeLabel.setFontScale(1 / Consts.ORGANISMS_STAGE_ZOOM);
        placeLabel.setPosition(pad, Consts.ORGANISMS_STAGE_CELL_SIZE - pad - spacingY * 0.5f);
        addActor(placeLabel);

        fitnessLabel.setFontScale(1 / Consts.ORGANISMS_STAGE_ZOOM);
        fitnessLabel.setPosition(pad, Consts.ORGANISMS_STAGE_CELL_SIZE - pad - spacingY * 1.5f);
        addActor(fitnessLabel);

        speciesLabel.setFontScale(1 / Consts.ORGANISMS_STAGE_ZOOM);
        speciesLabel.setPosition(pad, Consts.ORGANISMS_STAGE_CELL_SIZE - pad - spacingY * 2.7f);
        addActor(speciesLabel);

        parentIdLabel.setFontScale(1 / Consts.ORGANISMS_STAGE_ZOOM);
        parentIdLabel.setPosition(pad, Consts.ORGANISMS_STAGE_CELL_SIZE - pad - spacingY * 3.9f);
        addActor(parentIdLabel);

        clickListener = new ClickListener() {
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                super.enter(event, x, y, pointer, fromActor);
                if (pointer != -1) {
                    return;
                }
                overviewScreen.setShowWorld(true);
                overviewScreen.getWorldStage().init(organismGroup.getOrganism().getOrganismDef());
                updateScreenPosition(x, y);
            }

            @Override
            public boolean mouseMoved(InputEvent event, float x, float y) {
                updateScreenPosition(x, y);
                return super.mouseMoved(event, x, y);
            }

            private void updateScreenPosition(float x, float y) {
                Viewport worldViewport = overviewScreen.getWorldStage().getViewport();
                Viewport organismsViewport = overviewScreen.getOrganismsStage().getViewport();

                Vector2 cursor = new Vector2(x, y);
                localToStageCoordinates(cursor);
                overviewScreen.getOrganismsStage().stageToScreenCoordinates(cursor);

                cursor.x += worldViewport.getScreenWidth() * 0.1f;
                cursor.y *= -1f;
                cursor.y += worldViewport.getScreenHeight();

                if (cursor.x + worldViewport.getScreenWidth() > organismsViewport.getRightGutterX()) {
                    cursor.x -= worldViewport.getScreenWidth() * 1.2f;
                }
                float overTop = (cursor.y + worldViewport.getScreenHeight()) - organismsViewport.getTopGutterY();
                if (overTop > 0) {
                    cursor.y -= overTop;
                } else {
                    float underBottom = cursor.y - organismsViewport.getBottomGutterHeight();
                    if (underBottom < 0) {
                        cursor.y -= underBottom;
                    }
                }

                worldViewport.setScreenPosition((int) cursor.x, (int) cursor.y);
                overviewScreen.getWorldGuiStage().getViewport().setScreenPosition((int) cursor.x, (int) cursor.y);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                super.exit(event, x, y, pointer, toActor);
                if (pointer != -1) {
                    return;
                }
                overviewScreen.setShowWorld(false);
            }
        };
        addListener(clickListener);
    }

    public void init(OrganismGroup organismGroup, int place) {
        if (this.organismGroup != null) {
            this.organismGroup.remove();
        }
        this.organismGroup = organismGroup;
        this.organismGroup.setPosition(0, -4f);
        addActorAt(0, this.organismGroup);

        OrganismDef organismDef = this.organismGroup.getOrganism().getOrganismDef();

        placeLabel.setText(place + ".");

        String fitness = Assets.getDecimalFormat().format(organismDef.getFitness());
        fitnessLabel.setText(fitness + "m");
        fitnessLabel.setColor(organismDef.getFitness() == Consts.DISQUALIFIED_FITNESS_VALUE ? disqualified : blank);

        speciesLabel.setText("[#" + bodyPartColorHex + "]" + organismDef.getBodyPartDefCount() +
                             "[#" + springJointColorHex + "]" + organismDef.getSpringJointDefCount() +
                             "[#" + boneJointColorHex + "]" + organismDef.getBoneJointDefCount() +
                             "[#" + muscleJointColorHex + "]" + organismDef.getMuscleJointDefCount());

        if (organismDef.getParentId() == -1) {
            parentIdLabel.setText("");
        } else {
            parentIdLabel.setText("Child of " + (organismDef.getParentId() + 1) + ".");
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (clickListener.isOver()) {
            batch.setColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a * parentAlpha);
            batch.draw(background, getX(), getY(), getWidth(), getHeight());
        }
    }
}

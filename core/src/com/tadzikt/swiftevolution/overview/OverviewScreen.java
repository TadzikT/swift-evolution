package com.tadzikt.swiftevolution.overview;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.tadzikt.swiftevolution.App;
import com.tadzikt.swiftevolution.world.WorldGuiStage;
import com.tadzikt.swiftevolution.world.WorldStage;

public class OverviewScreen extends ScreenAdapter {
    private OverviewGuiStage overviewGuiStage;
    private OrganismsStage organismsStage;
    private boolean showOrganismsStage = false;
    private WorldStage worldStage;
    private WorldGuiStage worldGuiStage;
    private boolean showWorld = false;

    public OverviewScreen() {
        overviewGuiStage = new OverviewGuiStage(this);
        organismsStage = new OrganismsStage(this);
        worldStage = new WorldStage();
        worldGuiStage = new WorldGuiStage(worldStage);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(new InputMultiplexer(App.getInstance().getGlobalInputAdapter(), organismsStage,
                overviewGuiStage, worldGuiStage, worldStage));
    }


    @Override
    public void render(float delta) {
        overviewGuiStage.act(delta);
        overviewGuiStage.getViewport().apply();
        overviewGuiStage.draw();
        if (showOrganismsStage) {
            organismsStage.act(delta);
            overviewGuiStage.getViewport().apply();
            organismsStage.draw();
            if (showWorld) {
                worldStage.act(delta);
                worldStage.getViewport().setScreenSize((int) (organismsStage.getViewport().getScreenHeight() / 1.5f),
                        (int) (organismsStage.getViewport().getScreenHeight() / 1.5f));
                worldStage.getViewport().apply();
                worldStage.draw();
                worldGuiStage.act(delta);
                worldGuiStage.getViewport().setScreenSize((int) (organismsStage.getViewport().getScreenHeight() / 1.5f),
                        (int) (organismsStage.getViewport().getScreenHeight() / 1.5f));
                worldGuiStage.getViewport().apply();
                worldGuiStage.draw();
            }
        }
    }

    @Override
    public void resize(int width, int height) {
        overviewGuiStage.getViewport().update(width, height);
        organismsStage.getViewport().update(width, height);
        worldStage.getViewport().setScreenSize((int) (organismsStage.getViewport().getScreenHeight() / 1.5f),
                (int) (organismsStage.getViewport().getScreenHeight() / 1.5f));
        worldGuiStage.getViewport().setScreenSize((int) (organismsStage.getViewport().getScreenHeight() / 1.5f),
                (int) (organismsStage.getViewport().getScreenHeight() / 1.5f));
    }

    @Override
    public void dispose() {
        organismsStage.dispose();
        overviewGuiStage.dispose();
        worldStage.dispose();
        worldGuiStage.dispose();
    }

    public OrganismsStage getOrganismsStage() {
        return organismsStage;
    }

    public boolean isShowOrganismsStage() {
        return showOrganismsStage;
    }

    public void setShowOrganismsStage(boolean showOrganismsStage) {
        this.showOrganismsStage = showOrganismsStage;
    }

    public WorldStage getWorldStage() {
        return worldStage;
    }

    public WorldGuiStage getWorldGuiStage() {
        return worldGuiStage;
    }

    public void setShowWorld(boolean showWorld) {
        this.showWorld = showWorld;
    }
}
